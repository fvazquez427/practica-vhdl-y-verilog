library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Deco_2a4_conH is

	port(	in_ch		: in std_logic_vector(1 downto 0);
				en_ch		: in std_logic;
				out_ch	: out std_logic_vector(3 downto 0));

end entity Deco_2a4_conH;

--Arquitectura
architecture Deco_2a4_conH_arch of Deco_2a4_conH is
begin
	--Uso de sentencias secuenciales de ruteo.
	process(in_ch, en_ch) begin
		if(en_ch = '0') then
			out_ch <= "0000";
		elsif (in_ch = "00") then
			out_ch <= "0001";
		elsif (in_ch = "01") then
			out_ch <= "0010";
		elsif (in_ch = "10") then
			out_ch <= "0100";
		else
			out_ch <= "1000"; --in_ch = "11" y en_ch = '1'.
		end if;
	end process;
end architecture Deco_2a4_conH_arch;
