library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Cod_cp_4a3 is

	port(	in_ch		: in std_logic_vector(4 downto 1);
				out_ch	: out std_logic_vector(2 downto 0));

end entity Cod_cp_4a3;

--Arquitectura
architecture Cod_cp_4a3_arch of Cod_cp_4a3 is
begin
	--Uso de sentencias secuenciales de ruteo.
	process(in_ch) begin
		case (in_ch) is
			when "1000" | "1001" | "1010" | "1011" |
					 "1100" | "1101" | "1110" | "1111" => out_ch <= "100";
			when "0100" | "0101" | "0110" | "0111" => out_ch <= "011";
			when "0010" | "0011" => out_ch <= "010";
			when "0001" => out_ch <= "001";
			when others => out_ch <= "000";
		end case;
	end process;
end architecture Cod_cp_4a3_arch;
