library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Deco_2a4_conH is

	port(	in_ch		: in std_logic_vector(1 downto 0);
				en_ch		: in std_logic;
				out_ch	: out std_logic_vector(3 downto 0));

end entity Deco_2a4_conH;

--Arquitectura
architecture Deco_2a4_conH_arch of Deco_2a4_conH is
begin
	--Uso de sentencias de asignación condicional de señales.
	out_ch <= "0000" when (en_ch = '0') else
						"0001" when (in_ch = "00") else
						"0010" when (in_ch = "01") else
						"0100" when (in_ch = "10") else
						"1000";
end architecture Deco_2a4_conH_arch;
