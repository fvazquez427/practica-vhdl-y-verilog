library ieee;                 --Biblioteca IEEE
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector

--Entidad del Testbench (vacío)
entity Cod_cp_4a3_tb is
end entity Cod_cp_4a3_tb;

--Arquitectura del Testbench
architecture Cod_cp_4a3_tb_arq of Cod_cp_4a3_tb is

  --Declaración de estímulos y señales de monitoreo
  signal in_test  : std_logic_vector(4 downto 1);
  signal out_test : std_logic_vector(2 downto 0);

begin

  --Instanciar e interconectar DUT
  Cod_cp_4a3_unit : entity work.Cod_cp_4a3(Cod_cp_4a3_arch)
    port map(in_ch => in_test, out_ch => out_test);

  --Proceso de generación de entradas
  process begin
    case in_test is
		when "0000" => in_test <= "0001";
		when "0001" => in_test <= "0010";
		when "0010" => in_test <= "0011";
		when "0011" => in_test <= "0100";
		when "0100" => in_test <= "0101";
		when "0101" => in_test <= "0110";
		when "0110" => in_test <= "0111";
		when "0111" => in_test <= "1000";
		when "1000" => in_test <= "1001";
		when "1001" => in_test <= "1010";
		when "1010" => in_test <= "1011";
		when "1011" => in_test <= "1100";
		when "1100" => in_test <= "1101";
		when "1101" => in_test <= "1110";
		when "1110" => in_test <= "1111";
		when others => in_test <= "0000";
		end case;

    wait for 100 ns;

  end process;

end architecture Cod_cp_4a3_tb_arq;
