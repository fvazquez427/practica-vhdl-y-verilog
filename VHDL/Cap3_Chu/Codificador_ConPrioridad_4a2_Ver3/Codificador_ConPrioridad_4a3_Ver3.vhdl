library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Cod_cp_4a3 is

	port(	in_ch		: in std_logic_vector(4 downto 1);
				out_ch	: out std_logic_vector(2 downto 0));

end entity Cod_cp_4a3;

--Arquitectura
architecture Cod_cp_4a3_arch of Cod_cp_4a3 is
begin
	--Uso de sentencias secuenciales de ruteo (if-elsif-else)
	process(in_ch) begin
		if(in_ch(4) = '1') then
			out_ch <= "100";
		elsif(in_ch(3) = '1') then
			out_ch <= "011";
		elsif(in_ch(2) = '1') then
			out_ch <= "010";
		elsif(in_ch(1) = '1') then
			out_ch <= "001";
		else
			out_ch <= "000"; --in_ch = "0000"
		end if;
	end process;
end architecture Cod_cp_4a3_arch;
