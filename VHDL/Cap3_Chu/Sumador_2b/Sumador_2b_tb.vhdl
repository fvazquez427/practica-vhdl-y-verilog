library ieee;                 --Biblioteca IEEE
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector
use ieee.numeric_std.all;      --Paquete para signed, unsigned y operadores sobrecargados.

--Entidad del Testbench (vacío)
entity Sum_2b_tb is
end entity Sum_2b_tb;

--Arquitectura del Testbench
architecture Sum_2b_tb_arq of Sum_2b_tb is

  --Declaración de estímulos y señales de monitoreo
  signal a_test, b_test, sum_test : std_logic_vector(1 downto 0);
  signal cout_test                : std_logic;

begin

  --Instanciar e interconectar DUT
  Sum_2b_unit : entity work.Sum_2b(Sum_2b_arch)
    port map(a => a_test, b => b_test, sum => sum_test, cout => cout_test);

  --Proceso de generación de entradas
  process begin
    case a_test is
		when "00" => a_test <= "01";
		when "01" => a_test <= "10";
		when "10" => a_test <= "11";
		when others => a_test <= "00";
                   case b_test is
                   when "00" => b_test <= "01";
                   when "01" => b_test <= "10";
                   when "10" => b_test <= "11";
                   when others => b_test <= "00";
                  end case;
		end case;

    wait for 100 ns;
  end process;
end architecture Sum_2b_tb_arq;
