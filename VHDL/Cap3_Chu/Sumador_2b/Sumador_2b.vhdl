library ieee; 								--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector
use ieee.numeric_std.all; 		--Paquete para signed, unsigned y operadores sobrecargados.

--Entidad
entity sum_2b is

	port(	a, b		: in std_logic_vector(1 downto 0);
 				sum			: out std_logic_vector(1 downto 0);
				cout		: out std_logic);

end entity sum_2b;

--Arquitectura
architecture sum_2b_arch of sum_2b is
	constant N : integer := 2;
	signal a_ext, b_ext, sum_ext : unsigned(N downto 0);
begin
	a_ext <= unsigned('0' & a);
	b_ext <= unsigned('0' & b);
	sum_ext <= a_ext + b_ext;
	sum <= std_logic_vector(sum_ext (N-1 downto 0));
	cout <= std_logic(sum_ext(N));
end architecture sum_2b_arch;
