library ieee;                 --Biblioteca IEEE
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector

--Entidad del Testbench (vacío)
entity Deco_2a4_conH_tb is
end entity Deco_2a4_conH_tb;

--Arquitectura del Testbench
architecture Deco_2a4_conH_tb_arq of Deco_2a4_conH_tb is

  --Declaración de estímulos y señales de monitoreo
  signal in_test  : std_logic_vector(2 downto 0);
  signal out_test : std_logic_vector(3 downto 0);

begin

  --Instanciar e interconectar DUT
  Deco_2a4_conH_unit : entity work.Deco_2a4_conH(Deco_2a4_conH_arch)
    port map(in_ch => in_test(1 downto 0), en_ch => in_test(2), out_ch => out_test);

  --Proceso de generación de entradas
  process begin
    case in_test is
		when "000" => in_test <= "001";
		when "001" => in_test <= "010";
		when "010" => in_test <= "011";
		when "011" => in_test <= "100";
		when "100" => in_test <= "101";
		when "101" => in_test <= "110";
		when "110" => in_test <= "111";
		when others => in_test <= "000";
		end case;

    wait for 100 ns;

  end process;

end architecture Deco_2a4_conH_tb_arq;
