library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Cod_cp_4a3 is

	port(	in_ch		: in std_logic_vector(4 downto 1);
				out_ch	: out std_logic_vector(2 downto 0));

end entity Cod_cp_4a3;

--Arquitectura
architecture Cod_cp_4a3_arch of Cod_cp_4a3 is
begin
	--Uso de sentencias de asignación condicional de señales.
	out_ch <= "100" when (in_ch(4) = '1') else
						"011" when (in_ch(3) = '1') else
						"010" when (in_ch(2) = '1') else
						"001" when (in_ch(1) = '1') else
						"000";
end architecture Cod_cp_4a3_arch;
