library ieee;                 --Biblioteca IEEE
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector
use ieee.numeric_std.all;      --Paquete para signed, unsigned y operadores sobrecargados.

--Entidad del Testbench (vacío)
entity Sum_2b_tb is
end entity Sum_2b_tb;

--Arquitectura del Testbench
architecture Sum_2b_tb_arq of Sum_2b_tb is

  --Declaración señales de monitoreo
  signal a_test, b_test, sum_test  : std_logic_vector(1 downto 0);
  signal cout_test : std_logic;

  --Declaración de record para interfaz de entrada.
  type inputData_r is record
  	aT   : std_logic_vector(1 downto 0);
	bT   : std_logic_vector(1 downto 0);
  end record inputData_r;

  --Constante para cantidad de casos de entrada.
  constant cantidadCasos : integer := 16;
  --Declaración de vector de datos de entrada e instanciación como constante.
  type inputDataVec is array (0 to (cantidadCasos-1)) of inputData_r;

  constant inputDataV : inputDataVec :=
  (
  	("00","00"),
	("00","01"),
	("00","10"),
	("00","11"),

	("01","00"),
	("01","01"),
	("01","10"),
	("01","11"),

	("10","00"),
	("10","01"),
	("10","10"),
	("10","11"),

	("11","00"),
	("11","01"),
	("11","10"),
	("11","11")
  );

  --Declaración del sumador.
  component sum_nb is
  generic (
	N : integer := 2
  );
  port (
	a    : in  std_logic_vector(N-1 downto 0);
	b    : in  std_logic_vector(N-1 downto 0);
	sum  : out std_logic_vector(N-1 downto 0);
	cout : out std_logic
  );
  end component;

begin

  --Instanciar e interconectar DUT
  Sum_2b_unit : sum_nb
	generic map (
	  N => 2
	)
	port map (
	  a    => a_test,
	  b    => b_test,
	  sum  => sum_test,
	  cout => cout_test
	);

  --Proceso de generación de entradas
  entradas : process begin
	cambioValoresEntrada : for i in 0 to (cantidadCasos-1) loop
	a_test <= inputDataV(i).aT;
	b_test <= inputDataV(i).bT;
    wait for 100 ns;
	end loop cambioValoresEntrada;
	wait;
  end process entradas;
end architecture Sum_2b_tb_arq;
