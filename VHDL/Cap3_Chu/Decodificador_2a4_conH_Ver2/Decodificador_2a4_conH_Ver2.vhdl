library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Deco_2a4_conH is

	port(	in_ch		: in std_logic_vector(1 downto 0);
				en_ch		: in std_logic;
				out_ch	: out std_logic_vector(3 downto 0));

end entity Deco_2a4_conH;

--Arquitectura
architecture Deco_2a4_conH_arch of Deco_2a4_conH is
	--Señal auxiliar
	signal in_tot : std_logic_vector(2 downto 0);
begin

	--Concatenación de todas las entradas.
	in_tot <= en_ch & in_ch;

	--Uso de sentencias de asignación exclusiva de señales.
	with in_tot select
	out_ch <= "0000" when "000" | "001" | "010" | "011",
						"0001" when "100",
						"0010" when "101",
						"0100" when "110",
						"1000" when others; --in_tot = "111"
end architecture Deco_2a4_conH_arch;
