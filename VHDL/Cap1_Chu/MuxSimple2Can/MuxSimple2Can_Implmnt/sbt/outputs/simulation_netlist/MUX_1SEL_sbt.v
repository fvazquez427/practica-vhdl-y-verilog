// ******************************************************************************

// iCEcube Netlister

// Version:            2017.08.27940

// Build Date:         Sep 12 2017 08:25:46

// File Generated:     Mar 12 2020 17:50:12

// Purpose:            Post-Route Verilog/VHDL netlist for timing simulation

// Copyright (C) 2006-2010 by Lattice Semiconductor Corp. All rights reserved.

// ******************************************************************************

// Verilog file for cell "MUX_1SEL" view "INTERFACE"

module MUX_1SEL (
    sel,
    out_ch,
    in2,
    in1);

    input sel;
    output out_ch;
    input in2;
    input in1;

    wire N__173;
    wire N__172;
    wire N__171;
    wire N__164;
    wire N__163;
    wire N__162;
    wire N__155;
    wire N__154;
    wire N__153;
    wire N__146;
    wire N__145;
    wire N__144;
    wire N__127;
    wire N__124;
    wire N__121;
    wire N__118;
    wire N__115;
    wire N__112;
    wire N__109;
    wire N__106;
    wire N__103;
    wire N__100;
    wire N__97;
    wire N__94;
    wire N__91;
    wire N__88;
    wire N__85;
    wire N__82;
    wire N__79;
    wire N__76;
    wire N__73;
    wire N__70;
    wire N__67;
    wire VCCG0;
    wire GNDG0;
    wire in2_c;
    wire sel_c;
    wire in1_c;
    wire out_ch_obuf_RNOZ0;
    wire _gnd_net_;

    IO_PAD in1_ibuf_iopad (
            .OE(N__173),
            .DIN(N__172),
            .DOUT(N__171),
            .PACKAGEPIN(in1));
    defparam in1_ibuf_preio.NEG_TRIGGER=1'b0;
    defparam in1_ibuf_preio.PIN_TYPE=6'b000001;
    PRE_IO in1_ibuf_preio (
            .PADOEN(N__173),
            .PADOUT(N__172),
            .PADIN(N__171),
            .CLOCKENABLE(),
            .DIN0(in1_c),
            .DIN1(),
            .DOUT0(),
            .DOUT1(),
            .INPUTCLK(),
            .LATCHINPUTVALUE(),
            .OUTPUTCLK(),
            .OUTPUTENABLE());
    IO_PAD in2_ibuf_iopad (
            .OE(N__164),
            .DIN(N__163),
            .DOUT(N__162),
            .PACKAGEPIN(in2));
    defparam in2_ibuf_preio.NEG_TRIGGER=1'b0;
    defparam in2_ibuf_preio.PIN_TYPE=6'b000001;
    PRE_IO in2_ibuf_preio (
            .PADOEN(N__164),
            .PADOUT(N__163),
            .PADIN(N__162),
            .CLOCKENABLE(),
            .DIN0(in2_c),
            .DIN1(),
            .DOUT0(),
            .DOUT1(),
            .INPUTCLK(),
            .LATCHINPUTVALUE(),
            .OUTPUTCLK(),
            .OUTPUTENABLE());
    IO_PAD out_ch_obuf_iopad (
            .OE(N__155),
            .DIN(N__154),
            .DOUT(N__153),
            .PACKAGEPIN(out_ch));
    defparam out_ch_obuf_preio.NEG_TRIGGER=1'b0;
    defparam out_ch_obuf_preio.PIN_TYPE=6'b011001;
    PRE_IO out_ch_obuf_preio (
            .PADOEN(N__155),
            .PADOUT(N__154),
            .PADIN(N__153),
            .CLOCKENABLE(),
            .DIN0(),
            .DIN1(),
            .DOUT0(N__79),
            .DOUT1(),
            .INPUTCLK(),
            .LATCHINPUTVALUE(),
            .OUTPUTCLK(),
            .OUTPUTENABLE());
    IO_PAD sel_ibuf_iopad (
            .OE(N__146),
            .DIN(N__145),
            .DOUT(N__144),
            .PACKAGEPIN(sel));
    defparam sel_ibuf_preio.NEG_TRIGGER=1'b0;
    defparam sel_ibuf_preio.PIN_TYPE=6'b000001;
    PRE_IO sel_ibuf_preio (
            .PADOEN(N__146),
            .PADOUT(N__145),
            .PADIN(N__144),
            .CLOCKENABLE(),
            .DIN0(sel_c),
            .DIN1(),
            .DOUT0(),
            .DOUT1(),
            .INPUTCLK(),
            .LATCHINPUTVALUE(),
            .OUTPUTCLK(),
            .OUTPUTENABLE());
    InMux I__28 (
            .O(N__127),
            .I(N__124));
    LocalMux I__27 (
            .O(N__124),
            .I(N__121));
    Span4Mux_v I__26 (
            .O(N__121),
            .I(N__118));
    Span4Mux_h I__25 (
            .O(N__118),
            .I(N__115));
    Odrv4 I__24 (
            .O(N__115),
            .I(in2_c));
    InMux I__23 (
            .O(N__112),
            .I(N__109));
    LocalMux I__22 (
            .O(N__109),
            .I(N__106));
    Span4Mux_v I__21 (
            .O(N__106),
            .I(N__103));
    Span4Mux_v I__20 (
            .O(N__103),
            .I(N__100));
    Sp12to4 I__19 (
            .O(N__100),
            .I(N__97));
    Odrv12 I__18 (
            .O(N__97),
            .I(sel_c));
    InMux I__17 (
            .O(N__94),
            .I(N__91));
    LocalMux I__16 (
            .O(N__91),
            .I(N__88));
    Span4Mux_v I__15 (
            .O(N__88),
            .I(N__85));
    Sp12to4 I__14 (
            .O(N__85),
            .I(N__82));
    Odrv12 I__13 (
            .O(N__82),
            .I(in1_c));
    IoInMux I__12 (
            .O(N__79),
            .I(N__76));
    LocalMux I__11 (
            .O(N__76),
            .I(N__73));
    IoSpan4Mux I__10 (
            .O(N__73),
            .I(N__70));
    Span4Mux_s3_h I__9 (
            .O(N__70),
            .I(N__67));
    Odrv4 I__8 (
            .O(N__67),
            .I(out_ch_obuf_RNOZ0));
    VCC VCC (
            .Y(VCCG0));
    GND GND (
            .Y(GNDG0));
    GND GND_Inst (
            .Y(_gnd_net_));
    defparam out_ch_obuf_RNO_LC_1_3_0.C_ON=1'b0;
    defparam out_ch_obuf_RNO_LC_1_3_0.SEQ_MODE=4'b0000;
    defparam out_ch_obuf_RNO_LC_1_3_0.LUT_INIT=16'b1011101110001000;
    LogicCell40 out_ch_obuf_RNO_LC_1_3_0 (
            .in0(N__127),
            .in1(N__112),
            .in2(_gnd_net_),
            .in3(N__94),
            .lcout(out_ch_obuf_RNOZ0),
            .ltout(),
            .carryin(_gnd_net_),
            .carryout(),
            .clk(_gnd_net_),
            .ce(),
            .sr(_gnd_net_));
endmodule // MUX_1SEL
