
IO_Tile_0_25



LogicTile_1_25



LogicTile_2_25



LogicTile_3_25



LogicTile_4_25



LogicTile_5_25



LogicTile_6_25



LogicTile_7_25



RAM_Tile_8_25



LogicTile_9_25



LogicTile_10_25



LogicTile_11_25



LogicTile_12_25



LogicTile_13_25



LogicTile_14_25



LogicTile_15_25



LogicTile_16_25



LogicTile_17_25



LogicTile_18_25



LogicTile_19_25



LogicTile_20_25



LogicTile_21_25



LogicTile_22_25



LogicTile_23_25



LogicTile_24_25



RAM_Tile_25_25



LogicTile_26_25



LogicTile_27_25



LogicTile_28_25



LogicTile_29_25



LogicTile_30_25



LogicTile_31_25



LogicTile_32_25



IO_Tile_33_25



IO_Tile_0_24



LogicTile_1_24



LogicTile_2_24



LogicTile_3_24



LogicTile_4_24



LogicTile_5_24



LogicTile_6_24



LogicTile_7_24



RAM_Tile_8_24



LogicTile_9_24



LogicTile_10_24



LogicTile_11_24



LogicTile_12_24



LogicTile_13_24



LogicTile_14_24



LogicTile_15_24



LogicTile_16_24



LogicTile_17_24



LogicTile_18_24



LogicTile_19_24



LogicTile_20_24



LogicTile_21_24



LogicTile_22_24



LogicTile_23_24



LogicTile_24_24



RAM_Tile_25_24



LogicTile_26_24



LogicTile_27_24



LogicTile_28_24



LogicTile_29_24



LogicTile_30_24



LogicTile_31_24



LogicTile_32_24



IO_Tile_33_24



IO_Tile_0_11

 (0 3)  (17 179)  (17 179)  Enable bit of Mux _out_links/OutMux5_0 => wire_io_cluster/io_0/D_IN_0 span4_horz_40
 (17 3)  (0 179)  (0 179)  IOB_0 IO Functioning bit
 (2 6)  (15 182)  (15 182)  IO control bit: IOLEFT_REN_0

 (3 9)  (14 185)  (14 185)  IO control bit: IOLEFT_IE_0



LogicTile_1_11

 (11 5)  (29 181)  (29 181)  routing T_1_11.sp4_h_l_40 <X> T_1_11.sp4_h_r_5


LogicTile_5_11

 (13 4)  (247 180)  (247 180)  routing T_5_11.sp4_h_l_40 <X> T_5_11.sp4_v_b_5
 (12 5)  (246 181)  (246 181)  routing T_5_11.sp4_h_l_40 <X> T_5_11.sp4_v_b_5


IO_Tile_0_9

 (13 1)  (4 145)  (4 145)  routing T_0_9.span4_horz_1 <X> T_0_9.span4_vert_b_0
 (14 1)  (3 145)  (3 145)  routing T_0_9.span4_horz_1 <X> T_0_9.span4_vert_b_0


LogicTile_1_9



LogicTile_2_9



LogicTile_3_9



LogicTile_4_9

 (8 2)  (188 146)  (188 146)  routing T_4_9.sp4_h_r_5 <X> T_4_9.sp4_h_l_36
 (10 2)  (190 146)  (190 146)  routing T_4_9.sp4_h_r_5 <X> T_4_9.sp4_h_l_36


LogicTile_5_9

 (26 0)  (260 144)  (260 144)  routing T_5_9.lc_trk_g3_5 <X> T_5_9.wire_logic_cluster/lc_0/in_0
 (28 0)  (262 144)  (262 144)  routing T_5_9.lc_trk_g2_7 <X> T_5_9.wire_logic_cluster/lc_0/in_1
 (29 0)  (263 144)  (263 144)  Enable bit of Mux _logic_cluster/lcb1_0 => lc_trk_g2_7 wire_logic_cluster/lc_0/in_1
 (30 0)  (264 144)  (264 144)  routing T_5_9.lc_trk_g2_7 <X> T_5_9.wire_logic_cluster/lc_0/in_1
 (32 0)  (266 144)  (266 144)  Enable bit of Mux _logic_cluster/lcb3_0 => lc_trk_g1_0 wire_logic_cluster/lc_0/in_3
 (34 0)  (268 144)  (268 144)  routing T_5_9.lc_trk_g1_0 <X> T_5_9.wire_logic_cluster/lc_0/in_3
 (36 0)  (270 144)  (270 144)  LC_0 Logic Functioning bit
 (37 0)  (271 144)  (271 144)  LC_0 Logic Functioning bit
 (38 0)  (272 144)  (272 144)  LC_0 Logic Functioning bit
 (39 0)  (273 144)  (273 144)  LC_0 Logic Functioning bit
 (41 0)  (275 144)  (275 144)  LC_0 Logic Functioning bit
 (43 0)  (277 144)  (277 144)  LC_0 Logic Functioning bit
 (46 0)  (280 144)  (280 144)  Enable bit of Mux _out_links/OutMux7_0 => wire_logic_cluster/lc_0/out sp4_h_l_5
 (27 1)  (261 145)  (261 145)  routing T_5_9.lc_trk_g3_5 <X> T_5_9.wire_logic_cluster/lc_0/in_0
 (28 1)  (262 145)  (262 145)  routing T_5_9.lc_trk_g3_5 <X> T_5_9.wire_logic_cluster/lc_0/in_0
 (29 1)  (263 145)  (263 145)  Enable bit of Mux _logic_cluster/lcb0_0 => lc_trk_g3_5 wire_logic_cluster/lc_0/in_0
 (30 1)  (264 145)  (264 145)  routing T_5_9.lc_trk_g2_7 <X> T_5_9.wire_logic_cluster/lc_0/in_1
 (37 1)  (271 145)  (271 145)  LC_0 Logic Functioning bit
 (39 1)  (273 145)  (273 145)  LC_0 Logic Functioning bit
 (14 4)  (248 148)  (248 148)  routing T_5_9.sp4_v_b_0 <X> T_5_9.lc_trk_g1_0
 (16 5)  (250 149)  (250 149)  routing T_5_9.sp4_v_b_0 <X> T_5_9.lc_trk_g1_0
 (17 5)  (251 149)  (251 149)  Enable bit of Mux _local_links/g1_mux_0 => sp4_v_b_0 lc_trk_g1_0
 (21 10)  (255 154)  (255 154)  routing T_5_9.sp4_v_t_26 <X> T_5_9.lc_trk_g2_7
 (22 10)  (256 154)  (256 154)  Enable bit of Mux _local_links/g2_mux_7 => sp4_v_t_26 lc_trk_g2_7
 (23 10)  (257 154)  (257 154)  routing T_5_9.sp4_v_t_26 <X> T_5_9.lc_trk_g2_7
 (21 11)  (255 155)  (255 155)  routing T_5_9.sp4_v_t_26 <X> T_5_9.lc_trk_g2_7
 (16 14)  (250 158)  (250 158)  routing T_5_9.sp4_v_t_16 <X> T_5_9.lc_trk_g3_5
 (17 14)  (251 158)  (251 158)  Enable bit of Mux _local_links/g3_mux_5 => sp4_v_t_16 lc_trk_g3_5
 (18 14)  (252 158)  (252 158)  routing T_5_9.sp4_v_t_16 <X> T_5_9.lc_trk_g3_5


LogicTile_6_9



LogicTile_7_9



RAM_Tile_8_9



LogicTile_9_9



LogicTile_10_9



LogicTile_11_9



LogicTile_12_9



LogicTile_13_9



LogicTile_14_9



LogicTile_15_9



LogicTile_16_9



LogicTile_17_9



LogicTile_18_9



LogicTile_19_9



LogicTile_20_9



LogicTile_21_9



LogicTile_22_9



LogicTile_23_9



LogicTile_24_9



RAM_Tile_25_9



LogicTile_26_9



LogicTile_27_9



LogicTile_28_9



LogicTile_29_9



LogicTile_30_9



LogicTile_31_9



LogicTile_32_9



IO_Tile_33_9



IO_Tile_0_8



LogicTile_1_8



LogicTile_2_8



LogicTile_3_8



LogicTile_4_8



LogicTile_5_8

 (11 2)  (245 130)  (245 130)  routing T_5_8.sp4_v_b_11 <X> T_5_8.sp4_v_t_39
 (12 3)  (246 131)  (246 131)  routing T_5_8.sp4_v_b_11 <X> T_5_8.sp4_v_t_39


LogicTile_6_8



LogicTile_7_8



RAM_Tile_8_8



LogicTile_9_8



LogicTile_10_8



LogicTile_11_8



LogicTile_12_8



LogicTile_13_8



LogicTile_14_8



LogicTile_15_8



LogicTile_16_8



LogicTile_17_8



LogicTile_18_8



LogicTile_19_8



LogicTile_20_8



LogicTile_21_8



LogicTile_22_8



LogicTile_23_8



LogicTile_24_8



RAM_Tile_25_8



LogicTile_26_8



LogicTile_27_8



LogicTile_28_8



LogicTile_29_8



LogicTile_30_8



LogicTile_31_8



LogicTile_32_8



IO_Tile_33_8



IO_Tile_0_6

 (16 0)  (1 96)  (1 96)  IOB_0 IO Functioning bit
 (17 3)  (0 99)  (0 99)  IOB_0 IO Functioning bit
 (4 4)  (13 100)  (13 100)  routing T_0_6.span4_vert_b_12 <X> T_0_6.lc_trk_g0_4
 (13 4)  (4 100)  (4 100)  routing T_0_6.lc_trk_g0_4 <X> T_0_6.wire_io_cluster/io_0/D_OUT_0
 (16 4)  (1 100)  (1 100)  IOB_0 IO Functioning bit
 (5 5)  (12 101)  (12 101)  routing T_0_6.span4_vert_b_12 <X> T_0_6.lc_trk_g0_4
 (7 5)  (10 101)  (10 101)  Enable bit of Mux _local_links/g0_mux_4 => span4_vert_b_12 lc_trk_g0_4
 (13 5)  (4 101)  (4 101)  Enable bit of Mux _io_cluster/in_mux0_1 => lc_trk_g0_4 wire_io_cluster/io_0/D_OUT_0
 (2 6)  (15 102)  (15 102)  IO control bit: IOLEFT_REN_0



IO_Tile_0_5

 (17 2)  (0 82)  (0 82)  Enable bit of Mux _out_links/OutMuxb_0 => wire_io_cluster/io_0/D_IN_0 span12_horz_8
 (17 3)  (0 83)  (0 83)  IOB_0 IO Functioning bit
 (2 6)  (15 86)  (15 86)  IO control bit: IOLEFT_REN_0

 (3 9)  (14 89)  (14 89)  IO control bit: IOLEFT_IE_0



LogicTile_2_5

 (2 4)  (74 84)  (74 84)  Enable bit of Mux _span_links/cross_mux_horz_6 => sp12_h_r_12 sp4_h_l_7


LogicTile_5_5

 (6 2)  (240 82)  (240 82)  routing T_5_5.sp4_h_l_42 <X> T_5_5.sp4_v_t_37


IO_Tile_0_4

 (17 3)  (0 67)  (0 67)  IOB_0 IO Functioning bit
 (17 5)  (0 69)  (0 69)  Enable bit of Mux _out_links/OutMuxc_0 => wire_io_cluster/io_0/D_IN_0 span12_horz_16
 (2 6)  (15 70)  (15 70)  IO control bit: IOLEFT_REN_0

 (3 9)  (14 73)  (14 73)  IO control bit: IOLEFT_IE_0



LogicTile_2_4

 (2 12)  (74 76)  (74 76)  Enable bit of Mux _span_links/cross_mux_horz_10 => sp12_h_r_20 sp4_h_l_11


LogicTile_5_4

 (12 15)  (246 79)  (246 79)  routing T_5_4.sp4_h_l_46 <X> T_5_4.sp4_v_t_46

