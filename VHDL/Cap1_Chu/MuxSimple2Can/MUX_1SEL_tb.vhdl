library ieee;                 --Estándar IEEE 
use ieee.std_logic_1164.all;  --Biblioteca std_logic
use ieee.numeric_std.all;     --Biblioteca de formatos numéricos

--Entidad del Testbench (vacío)
entity MUX_1SEL_tb is
end entity MUX_1SEL_tb;

--Arquitectura del Testbench
architecture MUX_1SEL_tb_arq of MUX_1SEL_tb is

--DECLARAR ESTIMULOS

--Entradas al MUX
signal inp1   : std_logic;
signal inp2   : std_logic;
signal selec : std_logic;

--SALIDAS al MUX
signal out_mux : std_logic;

--DEFINIR CONSTANTES
constant PERIODO : time := 100 ns;

--DECLARAR DUT
component MUX_1SEL is
    port(   in1     : in  std_logic ;
            in2     : in  std_logic ;
            sel     : in  std_logic ;
            out_ch  : out std_logic);
end component;  

begin

--INSTANCIAR E INTERCONECTAR DUT
dut : MUX_1SEL port map(in1=>inp1,in2=>inp2,sel=>selec,out_ch=>out_mux);

--ESTIMULOS
    
--Proceso de generación de clock

--Proceso de generación de entradas
process begin
    inp1 <= '0';
    inp2 <= '0';
    selec <= '0';
    wait for 5*PERIODO;

    inp1 <= '1';
    inp2 <= '0';
    selec <= '0';
    wait for 5*PERIODO;

    inp1 <= '0';
    inp2 <= '1';
    selec <= '0';
    wait for 5*PERIODO;

    inp1 <= '1';
    inp2 <= '1';
    selec <= '0';
    wait for 5*PERIODO;

    inp1 <= '0';
    inp2 <= '0';
    selec <= '1';
    wait for 5*PERIODO;

    inp1 <= '1';
    inp2 <= '0';
    selec <= '1';
    wait for 5*PERIODO;

    inp1 <= '0';
    inp2 <= '1';
    selec <= '1';
    wait for 5*PERIODO;

    inp1 <= '1';
    inp2 <= '1';
    selec <= '1';
    wait for 5*PERIODO;

end process;

end architecture MUX_1SEL_tb_arq;

