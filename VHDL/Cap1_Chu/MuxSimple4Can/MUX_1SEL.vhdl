--Mux simple de 2 canales y con línea de habilitación

library IEEE; --Biblioteca
use IEEE.std_logic_1164.all; --Paquete para std_logic  

--Declaración de entidad
entity MUX_1SEL is

    --Puertos de E/S
    port(   in1, in2, sel, h	: in  std_logic ;
            out_ch  		: out std_logic);

end entity MUX_1SEL ;

--Declaración de arquitectura
architecture MUX_1SEL_arch of MUX_1SEL is

begin
	out_ch <= '0' when h='1' else
		  (in1 and not sel) or (in2 and sel); 

end architecture MUX_1SEL_arch ;
