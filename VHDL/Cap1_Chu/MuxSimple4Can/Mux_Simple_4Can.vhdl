--Mux simple de 4 canales implementado con expansión vertical.

library IEEE; --Biblioteca
use IEEE.std_logic_1164.all; --Paquete para std_logic  

--Declaración de entidad
entity Mux_Simple_4Can is

    --Puertos de E/S
    port(   inp1, inp2, inp3, inp4	: in  std_logic ;
	    selec 			: in std_logic_vector (1 downto 0);
            q	  			: out std_logic);

end entity Mux_Simple_4Can ;

--Declaración de arquitectura
architecture Mux_Simple_4Can_arch of Mux_Simple_4Can is
	
	--Señales
	signal inv_h, mux1_out, mux2_out: std_logic;
 
begin
	--Instanciación de 2 mux simples de 2 canales
	mux_Sy2can_dut0 : entity work.MUX_1SEL(MUX_1SEL_arch)
		port map (in1 => inp1, in2 => inp2, sel => selec(0), h => selec(1), out_ch => mux1_out);

	mux_Sy2can_dut1 : entity work.MUX_1SEL(MUX_1SEL_arch)
		port map (in1 => inp3, in2 => inp4, sel => selec(0), h => inv_h, out_ch => mux2_out);

	--Manejo de la línea de habilitación
	inv_h <= not selec(1);

	--Salida del mux final.
	q <= mux1_out or mux2_out;

end architecture Mux_Simple_4Can_arch ;
