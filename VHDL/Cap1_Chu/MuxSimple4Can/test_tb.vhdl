library ieee;                 --Estándar IEEE 
use ieee.std_logic_1164.all;  --Biblioteca std_logic
use ieee.numeric_std.all;     --Biblioteca de formatos numéricos

--Entidad del Testbench (vacío)
entity test_tb is
end entity test_tb;

--Arquitectura del Testbench
architecture test_tb_arch of test_tb is

	--DECLARAR ESTIMULOS
	signal inp1_test, inp2_test, inp3_test, inp4_test   : std_logic;
	signal selec_test : std_logic_vector (1 downto 0);
	signal out_test : std_logic;

begin

	--INSTANCIAR E INTERCONECTAR DUT
	mux_dut : entity work.Mux_Simple_4Can(Mux_Simple_4Can_arch)
		port map( inp1 => inp1_test,
			  inp2 => inp2_test,
			  inp3 => inp3_test,
			  inp4 => inp4_test,
			  selec => selec_test,
			  q => out_test);

	--ESTIMULOS
	    
	--Proceso de generación de entradas
	process begin
	
		--Modificación de habilitación en cada ciclo.
		case selec_test is 
			when "00" => selec_test <= "01";
			when "01" => selec_test <= "10";
			when "10" => selec_test <= "11";
			when others => selec_test <= "00";
		end case;

		--Secuencia de prueba
		inp1_test <= '0';
		inp2_test <= '0';
		inp3_test <= '0';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '0';
		inp3_test <= '0';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '1';
		inp3_test <= '0';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '1';
		inp3_test <= '0';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '0';
		inp3_test <= '1';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '0';
		inp3_test <= '1';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '1';
		inp3_test <= '1';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '1';
		inp3_test <= '1';
		inp4_test <= '0';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '0';
		inp3_test <= '0';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '0';
		inp3_test <= '0';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '1';
		inp3_test <= '0';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '1';
		inp3_test <= '0';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '0';
		inp3_test <= '1';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '0';
		inp3_test <= '1';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '0';
		inp2_test <= '1';
		inp3_test <= '1';
		inp4_test <= '1';
		wait for 100 ns;

		inp1_test <= '1';
		inp2_test <= '1';
		inp3_test <= '1';
		inp4_test <= '1';
		wait for 100 ns;

	end process;

end architecture test_tb_arch;

