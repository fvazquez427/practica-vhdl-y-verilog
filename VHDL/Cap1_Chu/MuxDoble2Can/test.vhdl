library ieee; --Biblioteca
use ieee.std_logic_1164.all; --Paquete para uso de tipo de dato std_logic y std_logic_vector

--Declaración de la entidad
entity test_tb is
end test_tb;

--Declaración de la arquitectura
architecture test_tb_arch of test_tb is 
	
	--Declaración de señales auxiliares de estímulo y monitoreo
	signal a_test, b_test : std_logic_vector (1 downto 0);
	signal se_test : std_logic;
	signal out_test : std_logic_vector (1 downto 0);
begin 
	
	--Se instancian un MUX doble de 2 canales
	mux_doble_dut : entity work.MuxDoble2Can(MuxDoble2Can_arch)
		port map ( a => a_test, b => b_test, selec => se_test, abq => out_test);

	--Se generan las secuencias de estímulo
	process begin
		
		--Cambio de canal cada vez que se arranca el loop.
		if(se_test = '0') then
			se_test <= '1';
		else
			se_test <= '0';
		end if;

		--Secuencia.
		a_test <= "00";
		b_test <= "00";
		wait for 100 ns;

		a_test <= "01";
		b_test <= "00";
		wait for 100 ns;

		a_test <= "10";
		b_test <= "00";
		wait for 100 ns;

		a_test <= "11";
		b_test <= "00";
		wait for 100 ns;

		a_test <= "00";
		b_test <= "01";
		wait for 100 ns;

		a_test <= "01";
		b_test <= "01";
		wait for 100 ns;

		a_test <= "10";
		b_test <= "01";
		wait for 100 ns;

		a_test <= "11";
		b_test <= "01";
		wait for 100 ns;

		a_test <= "00";
		b_test <= "10";
		wait for 100 ns;

		a_test <= "01";
		b_test <= "10";
		wait for 100 ns;

		a_test <= "10";
		b_test <= "10";
		wait for 100 ns;

		a_test <= "11";
		b_test <= "10";
		wait for 100 ns;

		a_test <= "00";
		b_test <= "11";
		wait for 100 ns;

		a_test <= "01";
		b_test <= "11";
		wait for 100 ns;

		a_test <= "10";
		b_test <= "11";
		wait for 100 ns;

		a_test <= "11";
		b_test <= "11";
		wait for 100 ns;

	end process;

end architecture test_tb_arch;
