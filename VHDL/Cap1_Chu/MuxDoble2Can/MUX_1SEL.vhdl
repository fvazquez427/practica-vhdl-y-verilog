library IEEE;
use IEEE.std_logic_1164.all;  

entity MUX_1SEL is

    --Puertos de E/S
    port(   in1     : in  std_logic ;
            in2     : in  std_logic ;
            sel     : in  std_logic ;
            out_ch  : out std_logic);

end entity MUX_1SEL ;

architecture ARQ_MUX_1SEL of MUX_1SEL is

begin

    out_ch <= (in1 and not sel) or (in2 and sel); 

end architecture ARQ_MUX_1SEL ;
