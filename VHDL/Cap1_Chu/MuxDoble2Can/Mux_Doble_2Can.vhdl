library ieee; --Biblioteca
use ieee.std_logic_1164.all; --Paquete para uso de tipo de dato std_logic y std_logic_vector

--Declaración de la entidad
entity MuxDoble2Can is
	port( 
		a,b	: in std_logic_vector (1 downto 0);
		selec	: in std_logic;
		abq 	: out std_logic_vector (1 downto 0));

end entity MuxDoble2Can;

--Declaración de la arquitectura
architecture MuxDoble2Can_arch of MuxDoble2Can is 

begin 
	
	--Se instancian dos MUX simples de 2 canales
	mux_simple_b0 : entity work.MUX_1SEL(ARQ_MUX_1SEL)
		port map ( in1 => a(0), in2 => b(0), sel => selec, out_ch => abq(0) );

	mux_simple_b1 : entity work.MUX_1SEL(ARQ_MUX_1SEL)
		port map ( in1 => a(1), in2 => b(1), sel => selec, out_ch => abq(1) );

end architecture MuxDoble2Can_arch;
