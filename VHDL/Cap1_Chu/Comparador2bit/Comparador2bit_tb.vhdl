library ieee;                 --Estándar IEEE 
use ieee.std_logic_1164.all;  --Biblioteca std_logic

--Entidad del Testbench (vacío)
entity Comp2bit_tb is
end entity Comp2bit_tb;

--Arquitectura del Testbench
architecture Comp2bit_tb_arq of Comp2bit_tb is

	--Declaración de señales estímulo
	signal inp1, inp2   : std_logic_vector (1 downto 0);
	signal out_comp : std_logic;

begin

	--Instanciación del dut
	comp2bit_unit : entity work.comp2bit(comp2bit_arch2)
		port map (a=>inp1, b=>inp2, abq=>out_comp);

    
	--Proceso de generación de entradas
	process begin
		inp1 <= "00";
		inp2 <= "00";
		wait for 100 ns;

		inp1 <= "01";
		inp2 <= "00";
		wait for 100 ns;

		inp1 <= "10";
		inp2 <= "00";
		wait for 100 ns;

		inp1 <= "11";
		inp2 <= "00";
		wait for 100 ns;

		inp1 <= "00";
		inp2 <= "01";
		wait for 100 ns;

		inp1 <= "01";
		inp2 <= "01";
		wait for 100 ns;

		inp1 <= "10";
		inp2 <= "01";
		wait for 100 ns;

		inp1 <= "11";
		inp2 <= "01";
		wait for 100 ns;

		inp1 <= "00";
		inp2 <= "10";
		wait for 100 ns;

		inp1 <= "01";
		inp2 <= "10";
		wait for 100 ns;

		inp1 <= "10";
		inp2 <= "10";
		wait for 100 ns;

		inp1 <= "11";
		inp2 <= "10";
		wait for 100 ns;

		inp1 <= "00";
		inp2 <= "11";
		wait for 100 ns;

		inp1 <= "01";
		inp2 <= "11";
		wait for 100 ns;

		inp1 <= "10";
		inp2 <= "11";
		wait for 100 ns;

		inp1 <= "11";
		inp2 <= "11";
		wait for 100 ns;

	end process;

end architecture Comp2bit_tb_arq;

