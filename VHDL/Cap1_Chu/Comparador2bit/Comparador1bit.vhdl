library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Comp1bit is

	port(	in1, in2 : in std_logic;
		out_ch : out std_logic);

end entity Comp1bit;

--Arquitectura
architecture Comp1bit_arch of Comp1bit is
	
	signal p0, p1 : std_logic; --Señales internas auxiliares para obtener los minitérminos.

begin
	
	p0 <= (not in1) and (not in2);
	p1 <= in1 and in2;

	out_ch <= p0 or p1; --Primer forma canónica.

end architecture Comp1bit_arch;
