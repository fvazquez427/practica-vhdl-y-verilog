--Ejemplo de descripción estructural (Sec. 1.3).

library ieee; --Biblioteca ieee.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector 

--Entidad
entity Comp2bit is

	port(	a, b : in std_logic_vector (1 downto 0);
		abq : out std_logic);

end entity Comp2bit;

architecture Comp2bit_arch2 of Comp2bit is
	
	signal e0, e1 : std_logic;

begin
	--Se instancian 2 comparadores de 1 bit.
	comp1bit_b0 : entity work.Comp1bit(Comp1bit_arch)
		port map (in1=>a(0), in2=>b(0), out_ch=>e0);

	comp1bit_b1 : entity work.Comp1bit(Comp1bit_arch)
		port map (in1=>a(1), in2=>b(1), out_ch=>e1);

	--Salida del comparador de 2 bits.
	abq <= e0 and e1;

end architecture Comp2bit_arch2;

--Arquitectura
