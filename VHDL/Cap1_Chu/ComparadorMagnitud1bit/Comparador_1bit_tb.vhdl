library ieee;                 --Biblioteca estándar IEEE
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector

--Entidad del testbench (vacío)
entity Comp1bit_tb is
end entity Comp1bit_tb;

--Arquitectura del testbench
architecture Comp1bit_tb_arq of Comp1bit_tb is

  --Declaración de estímulos y señal de monitoreo.

  --Entradas al comparador
  signal in1_t, in2_t   : std_logic;

  --Salidas del comparador
  signal out_eq_t, out_gt_t, out_lt_t : std_logic;

  begin
      --Instanciación del DUT
      Comp1bit_unit : entity work.Comp1bit(Comp1bit_arch)
        port map(in1 => in1_t, in2 => in2_t, out_eq => out_eq_t, out_gt => out_gt_t, out_lt => out_lt_t);

      --Proceso de generación de entradas
      process begin
          in1_t <= '0';
          in2_t <= '0';
          wait for 100 ns;

          in1_t <= '0';
          in2_t <= '1';
          wait for 100 ns;

          in1_t <= '1';
          in2_t <= '0';
          wait for 100 ns;

          in1_t <= '1';
          in2_t <= '1';
          wait for 100 ns;
      end process;

end architecture Comp1bit_tb_arq;
