library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Comp2bit is

	port(	a, b : in std_logic_vector (1 downto 0);
		abq : out std_logic);

end entity Comp2bit;

--Arquitectura
architecture Comp2bit_arch of Comp2bit is
	
	signal p0, p1, p2, p3 : std_logic; --Señales internas auxiliares para obtener los minitérminos.

begin
	
	p0 <= (not a(0)) and (not a(1)) and (not b(0)) and (not b(1));
	p1 <= a(0) and (not a(1)) and b(0) and (not b(1));
	p2 <= (not a(0)) and a(1) and (not b(0)) and b(1);
	p3 <= a(0) and a(1) and b(0) and b(1);

	abq <= p0 or p1 or p2 or p3 ; --Primer forma canónica.

end architecture Comp2bit_arch;
