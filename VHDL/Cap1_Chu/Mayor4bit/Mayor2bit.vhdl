library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Mayor2bit is

	port(	a, b : in std_logic_vector (1 downto 0);
		abq : out std_logic);

end entity Mayor2bit;

--Arquitectura
architecture Mayor2bit_arch of Mayor2bit is
	
	signal p0, p1, p2, p3, p4, p5 : std_logic; --Señales internas auxiliares para obtener los minitérminos.

begin
	
	p0 <= (not a(1)) and a(0) and (not b(1)) and (not b(0));
	p1 <= a(1) and (not a(0)) and (not b(1)) and (not b(0));
	p2 <= a(1) and (not a(0)) and (not b(1)) and b(0);
	p3 <= a(1) and a(0) and (not b(1)) and (not b(0));
	p4 <= a(1) and a(0) and (not b(1)) and b(0);
	p5 <= a(1) and a(0) and b(1) and (not b(0));
	
	abq <= p0 or p1 or p2 or p3 or p4 or p5 ; --Primer forma canónica.

end architecture Mayor2bit_arch;
