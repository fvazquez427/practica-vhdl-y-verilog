library ieee; --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector

--Entidad
entity Mayor4bit is

	port(	in1, in2 : in std_logic_vector (3 downto 0);	
		out_ch : out std_logic);

end entity Mayor4bit;

--Arquitectura
architecture Mayor4bit_arch of Mayor4bit is
	
	--Señales auxiliares	
	signal e0, e1, e2 : std_logic; --Señales internas auxiliares para obtener los minitérminos.

begin
	--Instanciación de componentes
	mayor2bit_unit0 : entity work.Mayor2bit(Mayor2bit_arch)
		port map (a => in1(3 downto 2), b => in2(3 downto 2), abq => e0);

	mayor2bit_unit1 : entity work.Mayor2bit(Mayor2bit_arch)
		port map (a => in1(1 downto 0), b => in2(1 downto 0), abq => e2);

	comp2bit_unit0 : entity work.Comp2bit(Comp2bit_arch)
		port map (a => in1(3 downto 2), b => in2(3 downto 2), abq => e1);

	out_ch <= e0 or ( e1 and e2 );

end architecture Mayor4bit_arch;
