library ieee;                 --Estándar IEEE 
use ieee.std_logic_1164.all;  --Biblioteca std_logic

--Entidad del Testbench (vacío)
entity Mayor4bit_tb is
end entity Mayor4bit_tb;

--Arquitectura del Testbench
architecture Mayor4bit_tb_arch of Mayor4bit_tb is

	--Declaración de señales estímulo
	signal in1_test, in2_test   : std_logic_vector (3 downto 0);
	signal out_test : std_logic;

begin

	--Instanciación del dut
	mayor4bit_unit : entity work.Mayor4bit(Mayor4bit_arch)
		port map (in1 => in1_test, in2 => in2_test, out_ch => out_test);

    
	--Proceso de generación de entradas
	process begin
		case in2_test is 
		when "0000" => in2_test <= "0001";
		when "0001" => in2_test <= "0010";
		when "0010" => in2_test <= "0011";
		when "0011" => in2_test <= "0100";
		when "0100" => in2_test <= "0101";
		when "0101" => in2_test <= "0110";
		when "0110" => in2_test <= "0111";
		when "0111" => in2_test <= "1000";
		when "1000" => in2_test <= "1001";
		when "1001" => in2_test <= "1010";
		when "1010" => in2_test <= "1011";
		when "1011" => in2_test <= "1100";
		when "1100" => in2_test <= "1101";
		when "1101" => in2_test <= "1110";
		when "1110" => in2_test <= "1111";
		when others => in2_test <= "0000";
		end case;
		
		in1_test <= "0000";
		wait for 100 ns;

		in1_test <= "0001";
		wait for 100 ns;

		in1_test <= "0010";
		wait for 100 ns;

		in1_test <= "0011";
		wait for 100 ns;

		in1_test <= "0100";
		wait for 100 ns;

		in1_test <= "0101";
		wait for 100 ns;

		in1_test <= "0110";
		wait for 100 ns;

		in1_test <= "0111";
		wait for 100 ns;

		in1_test <= "1000";
		wait for 100 ns;

		in1_test <= "1001";
		wait for 100 ns;

		in1_test <= "1010";
		wait for 100 ns;

		in1_test <= "1011";
		wait for 100 ns;

		in1_test <= "1100";
		wait for 100 ns;

		in1_test <= "1101";
		wait for 100 ns;

		in1_test <= "1110";
		wait for 100 ns;

		in1_test <= "1111";
		wait for 100 ns;

	end process;

end architecture Mayor4bit_tb_arch;

