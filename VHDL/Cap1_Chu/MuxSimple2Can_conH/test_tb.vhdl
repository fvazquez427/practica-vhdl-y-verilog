library ieee;                 --Estándar IEEE 
use ieee.std_logic_1164.all;  --Biblioteca std_logic
use ieee.numeric_std.all;     --Biblioteca de formatos numéricos

--Entidad del Testbench (vacío)
entity MUX_1SEL_tb is
end entity MUX_1SEL_tb;

--Arquitectura del Testbench
architecture MUX_1SEL_tb_arch of MUX_1SEL_tb is

	--DECLARAR ESTIMULOS
	signal inp1, inp2, selec, h_test   : std_logic;
	signal out_mux : std_logic;

begin

	--INSTANCIAR E INTERCONECTAR DUT
	mux_dut : entity work.MUX_1SEL(MUX_1SEL_arch)
		port map( in1 => inp1, in2 => inp2, sel => selec, h => h_test, out_ch => out_mux);

	--ESTIMULOS
	    
	--Proceso de generación de entradas
	process begin
	
		--Modificación de habilitación en cada ciclo.
		if( h_test = '0') then
			h_test <= '1';
		else
			h_test <= '0';
		end if;

		--Secuencia de prueba
		inp1 <= '0';
		inp2 <= '0';
		selec <= '0';
		wait for 100 ns;

		inp1 <= '1';
		inp2 <= '0';
		selec <= '0';
		wait for 100 ns;

		inp1 <= '0';
		inp2 <= '1';
		selec <= '0';
		wait for 100 ns;

		inp1 <= '1';
		inp2 <= '1';
		selec <= '0';
		wait for 100 ns;

		inp1 <= '0';
		inp2 <= '0';
		selec <= '1';
		wait for 100 ns;

		inp1 <= '1';
		inp2 <= '0';
		selec <= '1';
		wait for 100 ns;

		inp1 <= '0';
		inp2 <= '1';
		selec <= '1';
		wait for 100 ns;

		inp1 <= '1';
		inp2 <= '1';
		selec <= '1';
		wait for 100 ns;

	end process;

end architecture MUX_1SEL_tb_arch;

