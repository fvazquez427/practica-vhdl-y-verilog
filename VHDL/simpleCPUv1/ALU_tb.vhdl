--------------------------------------------------------------------------------
--- Entidad: ALU_tb.
--- Descripción: Este testbench permite probar la ALU del Simple CPU v1.
--- Bibliografía: "Simple CPU v1", 9/10/2020,
--                https://creativecommons.org/licenses/by-nc-nd/4.0/
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 27/02/2021.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.

--Entidad del testbench.
entity ALU_tb is
end entity ALU_tb;

--Arquitectura del testbench.
architecture ALU_tb_arch of ALU_tb is
	--Declaración de la ALU a probar.
    component ALU
        generic (
            N_BITS : integer := 8
        );
        port (
            a_in    : in  std_logic_vector(N_BITS-1 downto 0);
            b_in    : in  std_logic_vector(N_BITS-1 downto 0);
            s0_in   : in  std_logic;
            s1_in   : in  std_logic;
            s2_in   : in  std_logic;
            s3_in   : in  std_logic;
            s4_in   : in  std_logic;
            z_out   : out std_logic_vector(N_BITS-1 downto 0);
            Car_out : out std_logic
        );
    end component ALU;

	--Declaración de constantes.
	constant DELAY    : time    := 100 ns;
    constant NUM_BITS : integer := 8;

    constant A1 : std_logic_vector := x"E8";
    constant B1 : std_logic_vector := x"22";

	--Declaración de estímulos y señal de monitoreo.
	--Entradas a la ALU.
    signal test_a_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_b_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_s0_s  : std_logic;
    signal test_s1_s  : std_logic;
    signal test_s2_s  : std_logic;
    signal test_s3_s  : std_logic;
    signal test_s4_s  : std_logic;

	--Salidas de la ALU.
    signal test_z_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_Car_s : std_logic;

begin
	--Instanciación del DUT.
    ALU_0 : ALU
        generic map (
            N_BITS => NUM_BITS
        )
        port map (
            a_in    => test_a_s,
            b_in    => test_b_s,
            s0_in   => test_s0_s,
            s1_in   => test_s1_s,
            s2_in   => test_s2_s,
            s3_in   => test_s3_s,
            s4_in   => test_s4_s,
            z_out   => test_z_s,
            Car_out => test_Car_s
        );

	--Proceso de aplicación de estímulos en la entrada del DUT.
	applyStimulus : process
	begin
		--Salida: b_in.
        test_s0_s <= '1';
        test_s1_s <= '1';
        test_s2_s <= '0';
        test_s3_s <= '0';
        test_s4_s <= '0';
        test_a_s  <= A1;
        test_b_s  <= B1;
		wait for DELAY;

        --Salida: a_in.
        test_s0_s <= '0';
        wait for DELAY;

        --Salida: a_in and b_in.
        test_s0_s <= '1';
        test_s1_s <= '0';
        wait for DELAY;

        --Salida: a_in + b_in + Cin (0).
        test_s0_s <= '0';
        wait for DELAY;

        --Salida: a_in + b_in + Cin (1).
        test_s2_s <= '1';
        wait for DELAY;

        --Salida: a_in + Cin (1).
        test_s4_s <= '1';
        wait for DELAY;

        --Salida: a_in - b_in (con Cin = 1).
        test_s4_s <= '0';
        test_s3_s <= '1';
        wait for DELAY;

		--Se detiene la simulación.
		wait;
	end process applyStimulus;
end architecture ALU_tb_arch;
