--------------------------------------------------------------------------------
--- Entidad: regSISO.
--- Descripción: Prueba de generación de registro SISO de n bits en base a un
---				 registro PIPO.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 10/07/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;					--Biblioteca estándar ieee.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.

--Declaración de la entidad.
entity regSISO is
	generic(
		nBitsReg 	  : integer := 8;
		riseEdgeClock : BOOLEAN := TRUE);
	port(
		dSISO_in 			: in  std_logic;
		resetSISO_in 		: in  std_logic;
		outEnableSISO_in	: in  std_logic;
		clockSISO_in 		: in  std_logic;
		qSISO_out 			: out std_logic);
end entity regSISO;

--Declaración de la arquitectura.
architecture regSISO_arch of regSISO is
	--Declaración del registro PIPO a usar.
	component regPIPO is
		generic (
			nBits           : integer := 8;
			risingEdgeClock : BOOLEAN := TRUE
		);
		port (
			d_in          	: in  std_logic_vector(nBits-1 downto 0);
			reset_in    	: in  std_logic;
			outEnable_in	: in  std_logic;
			clock_in        : in  std_logic;
			q_out         	: out std_logic_vector(nBits-1 downto 0)
		);
	end component;

	--Declaración de constantes y señales.
	signal inputReg_s : std_logic_vector(nBitsReg-1 downto 0);
	signal outputReg_s : std_logic_vector(nBitsReg-1 downto 0);

begin
	--Instanciación del registros PIPO.
	regPIPO_0 : regPIPO
		generic map (nBits => nBitsReg, risingEdgeClock => riseEdgeClock)
		port map ( d_in 			=> inputReg_s,
				   reset_in     	=> resetSISO_in,
				   outEnable_in		=> outEnableSISO_in,
				   clock_in       	=> clockSISO_in,
				   q_out           	=> outputReg_s );

	--Roteo de señales.
	inputReg_s(0) <= dSISO_in;
	inputReg_s(nBitsReg-1 downto 1) <= outputReg_s(nBitsReg-2 downto 0);
	qSISO_out <= outputReg_s(nBitsReg-1);
end architecture regSISO_arch;
