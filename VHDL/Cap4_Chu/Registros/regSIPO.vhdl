--------------------------------------------------------------------------------
--- Entidad: regSIPO.
--- Descripción: Prueba de generación de registro SIPO de n bits en base a un
---				 registro PIPO.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 10/07/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;					--Biblioteca estándar ieee.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.

--Declaración de la entidad.
entity regSIPO is
	generic(
		nBitsReg	  : integer := 8;
		riseEdgeClock : BOOLEAN := TRUE);
	port(
		dSIPO_in 			: in  std_logic;
		resetSIPO_in 		: in  std_logic;
		outEnableSIPO_in	: in  std_logic;
		clockSIPO_in 		: in  std_logic;
		qSIPO_out 			: out std_logic_vector(nBitsReg-1 downto 0));
end entity regSIPO;

--Declaración de la arquitectura.
architecture regSIPO_arch of regSIPO is
	--Declaración del registro PIPO a usar.
	component regPIPO is
		generic (
			nBits           : integer := 8;
			risingEdgeClock : BOOLEAN := TRUE
		);
		port (
			d_in          	: in  std_logic_vector(nBits-1 downto 0);
			reset_in    	: in  std_logic;
			outEnable_in	: in  std_logic;
			clock_in        : in  std_logic;
			q_out         	: out std_logic_vector(nBits-1 downto 0)
		);
	end component;

	--Declaración de constantes y señales.
	signal inputReg_s  : std_logic_vector(nBitsReg-1 downto 0);
	signal outputReg_s : std_logic_vector(nBitsReg-1 downto 0);

begin
	--Instanciación del registro PIPO.
	regPIPO_0 : regPIPO
		generic map (nBits => nBitsReg, risingEdgeClock => riseEdgeClock)
		port map ( d_in 			=> inputReg_s,
				   reset_in     	=> resetSIPO_in,
				   outEnable_in		=> outEnableSIPO_in,
				   clock_in       	=> clockSIPO_in,
				   q_out           	=> outputReg_s );

	--Roteo de señales.
	inputReg_s(0) <= dSIPO_in;
	inputReg_s(nBitsReg-1 downto 1) <= outputReg_s(nBitsReg-2 downto 0);
	qSIPO_out <= outputReg_s;
end architecture regSIPO_arch;
