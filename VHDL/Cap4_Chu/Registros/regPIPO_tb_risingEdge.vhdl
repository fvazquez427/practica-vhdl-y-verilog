--------------------------------------------------------------------------------
--- Entidad: regPIPO_tb_risingEdge.
--- Descripción: Esta entidad es un testbench que permite verificar el
--				 funcionamiento de un registro PIPO de 8 bits que se activa por
--				 flanco ascendente de clock y posee una línea asincrónica de
--				 habilitación de salida y otra de reset.
--- Propósito: Este testbench prueba de forma exhaustiva al registro. Se aplica
--			   la misma secuencia de 16 estímulos 3 veces, presentando las
--             siguientes particularidades: la primera es con el reset
--			   desactivado y la salida habilitada; la segunda es con el reset y
--			   salida activados (para probar el reset); y la tercera es con el
--			   reset desactivado y la salida deshabilitada (para probar esta
--			   habilitación y su mayor jerarquía).
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 11/07/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                 --Biblioteca estándar ieee.
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector.

--Entidad del testbench.
entity regPIPO_tb_risingEdge is
end entity regPIPO_tb_risingEdge;

--Arquitectura del testbench.
architecture regPIPO_tb_risingEdge_arch of regPIPO_tb_risingEdge is
	--Declaración del registro a probar.
	component regPIPO is
		generic (
			nBits           : integer := 8;
			risingEdgeClock : BOOLEAN := TRUE
		);
		port (
			d_in          	: in  std_logic_vector(nBits-1 downto 0);
			reset_in    	: in  std_logic;
			outEnable_in	: in  std_logic;
			clock_in        : in  std_logic;
			q_out         	: out std_logic_vector(nBits-1 downto 0)
		);
	end component;

	--Declaración de constantes.
	constant TESTED_NBITS : integer := 4;
	constant PERIOD 	  : time 	:= 100 ns;

	--Declaración de estímulos y señal de monitoreo.
	--Entradas al registro.
	signal test_d_reg_s    		: std_logic_vector(TESTED_NBITS-1 downto 0);
	signal test_reset_s     	: std_logic;
	signal test_outEnable_s 	: std_logic;
	signal test_clock_s     	: std_logic;

	--Salidas al registro.
	signal test_q_reg_s	: std_logic_vector(TESTED_NBITS-1 downto 0);

	--Señal auxiliar para detener la simulación (por defecto es FALSE).
	signal stopSimulation_s : BOOLEAN := FALSE;

	--Declaración de vector de std_logic_vector para agrupar en este los valores
	--de entrada al registro.
	type stimulus_vec is array (integer range <>) of
								std_logic_vector(TESTED_NBITS-1 downto 0);

	--Declaración de un vector vec_stimulus con los datos de entrada.
	constant IN_REGISTER : stimulus_vec := (
		("0000"), ("0001"), ("0010"), ("0011"),
		("0100"), ("0101"), ("0110"), ("0111"),
		("1000"), ("1001"), ("1010"), ("1011"),
		("1100"), ("1101"), ("1110"), ("1111")
	);

begin
	--Instanciación del DUT (Device Under Test).
	regPIPO_0 : regPIPO
   		generic map (nBits => TESTED_NBITS, risingEdgeClock => TRUE)
   		port map ( d_in 			=> test_d_reg_s,
     			   reset_in     	=> test_reset_s,
			       outEnable_in		=> test_outEnable_s,
			       clock_in       	=> test_clock_s,
			       q_out           	=> test_q_reg_s );

	--Proceso de generación de clock.
	clockGeneration : process
	begin
	    test_clock_s <= '1';
	    wait for PERIOD/2;
	    test_clock_s <= '0';
	    wait for PERIOD/2;
	    if (stopSimulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

	--Proceso de aplicación de estímulos.
   	applyStimulus : process
   	begin
		--Estado inicial: entrada toda en 0 y salida habilitada.
		test_d_reg_s 		<= (others=> '0');
		test_outEnable_s	<= '1';
		stopSimulation_s 	<= FALSE;

		--Reset inicial.
		test_reset_s <= '1';
		wait for 2*PERIOD; --El reset dura dos periodos de clock.
		test_reset_s <= '0';

		--Se agrega un desfasaje temporal inicial.
		wait for PERIOD/2;

		--Se aplican los estímulos en la entrada de datos del registro.
   		applyInputData_1: for i in IN_REGISTER'range loop
   			test_d_reg_s  <= IN_REGISTER(i);
   			wait for PERIOD;
   		end loop applyInputData_1;

		--Se hace un reset para poner la salida en cero y luego se reaplican los
		--estímulos en la entrada de datos. Aquí se prueba la asincronía y mayor
		--jerarquía de la entrada de reset.
		test_reset_s <= '1';

		applyInputData_2: for i in IN_REGISTER'range loop
			test_d_reg_s  <= IN_REGISTER(i)	;
			wait for PERIOD;
		end loop applyInputData_2;
		test_reset_s <= '0';

		--Se deshabilita la salida del registro y luego se reaplican los
		--estímulos en la entrada de datos. Aquí se prueba la asincronía y más
		--alta jerarquía de la entrada de habilitación de la salida.
		test_outEnable_s <= '0';
		applyInputData_3: for i in IN_REGISTER'range loop
			test_d_reg_s  <= IN_REGISTER(i)	;
			wait for PERIOD;
		end loop applyInputData_3;

		--Se detiene la simulación.
		stopSimulation_s <= TRUE;
   		wait;
   	end process applyStimulus;
end architecture regPIPO_tb_risingEdge_arch;
