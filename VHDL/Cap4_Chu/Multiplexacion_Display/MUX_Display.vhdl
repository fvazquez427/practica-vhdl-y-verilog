--------------------------------------------------------------------------------
--- Entidad: MUX_Display.
--- Descripción: Esta entidad es multiplexor temporal para 4 displays 7 de
--				 segmentos. Se usa un contador interno de nBits, donde los
--				 2 MSB se usan para seleccionar con un MUX que datos de entrada
--				 in0, in1, in2 o in3 se copian a la salida segData_out, y que
--				 display se activa, decodificando la señal a 4 bits y enviándola
--				 a segEnable_out.
--
--				La frecuencia de refresco de cada dígito es igual a:
--						TR = FrecIN / [2 ^(nBits-2)]
--
--				Para variar esta tasa se debe alterar el valor de nBits. Para la
--				EDU-CIAA-FPGA, con FrecIn de 12 MHz, se pone nBits igual a 16
--				para tener una TR de 732 Hz aproximadamente.
--
--				Esta entidad funciona con flanco ascendente de clock y tiene
--				una entrada de reset asincrónica que resetea el contador interno
--				y una entrada de habilitación que permite activar o no el
--				dispositivo o usarlo con una frecuencia de clock distinta a la
--				del sistema.
--
--				Basado en el libro de Pong. P. Chu "FPGA Prototyping by VHDL
--				examples", cap 4, ejemplo "Time Multiplexing with LED patterns",
--			 	Listing 4.13.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 23/08/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee y
--				  paquete numeric_std para uso de signed y unsigned.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 					--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;		--Paquete para signed y unsigned.

--Entidad
entity MUX_Display is
	generic(
		nBits : integer := 16
	);
	port(
		in0				: in std_logic_vector(7 downto 0);
		in1				: in std_logic_vector(7 downto 0);
		in2				: in std_logic_vector(7 downto 0);
		in3				: in std_logic_vector(7 downto 0);
		clock_in	    : in std_logic;
		reset_in		: in std_logic;
		enable_in		: in std_logic;
		segData_out		: out std_logic_vector(7 downto 0);
		segEnable_out	: out std_logic_vector(3 downto 0));
end entity MUX_Display;

--Arquitectura
architecture MUX_Display_arch of MUX_Display is
	--Señales auxiliares.
	signal Pulses_cnt     : unsigned (nBits-1 downto 0);
	signal segSelection_s : std_logic_vector (1 downto 0);
begin
	--Proceso de contado y reset.
	CountAndReset: process(clock_in, reset_in)
	begin
		if(reset_in = '1') then
			Pulses_cnt <= (others => '0');
		elsif(rising_edge(clock_in) and enable_in = '1') then
			Pulses_cnt <= (Pulses_cnt + 1);
		end if;
	end process CountAndReset;

	--Asignación de segSelection_s.
	segSelection_s <= std_logic_vector(Pulses_cnt(nBits-1 downto nBits-2));

	--Multiplexación de datos de entrada y habilitación de display.
	MultiplexingAndSegmentEnable: process(segSelection_s, in0, in1, in2, in3)
	begin
		case segSelection_s is
			when "00" =>
				segData_out   <= in0;
	    		segEnable_out <= "0001";
			when "01" =>
				segData_out   <= in1;
				segEnable_out <= "0010";
			when "10" =>
				segData_out   <= in2;
				segEnable_out <= "0100";
			when others =>
				segData_out   <= in3;
				segEnable_out <= "1000";
		end case;
	end process MultiplexingAndSegmentEnable;
end architecture MUX_Display_arch;
