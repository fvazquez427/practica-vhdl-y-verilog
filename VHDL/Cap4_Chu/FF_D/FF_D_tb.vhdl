library ieee;                 --Estándar IEEE
use ieee.std_logic_1164.all;  --Biblioteca std_logic
use ieee.numeric_std.all;     --Biblioteca de formatos numéricos

--Entidad del Testbench (vacío)
entity tb is
end entity tb;

--Arquitectura del Testbench
architecture tb_arq of tb is

--DECLARAR ESTIMULOS

--Entradas al FF
signal d   : std_logic;
signal en   : std_logic;
signal q   : std_logic;
signal rst : std_logic;
signal clk : std_logic;

--Lista de estimulos
signal stimulus : std_logic_vector(31 downto 0) := x"A0B0C0D0";

--Variable auxiliar para detener la simulación
signal stop_sig : std_logic;

--DEFINIR CONSTANTES
constant PERIODO : time := 100 ns;

--DECLARAR DUT
component FF_D_En is
  port (
	d_in      : in  std_logic;
	clk_in    : in  std_logic;
	reset_in  : in  std_logic;
	enable_in : in  std_logic;
	q_out     : out std_logic
  );
end component;

begin

--INSTANCIAR E INTERCONECTAR DUT
dut : FF_D_En port map(clk_in  =>clk,d_in=>d,reset_in=>rst,enable_in=>en,q_out=>q);

--ESTIMULOS

--Proceso de reset inicial:
process begin
    rst <= '1';
    wait for 2*PERIODO; --El reset dura dos periodos de clock
    rst <= '0';
    wait; --Bloquear al proceso
end process;

--Proceso de generación de clock
process begin
    clk <= '1';
    wait for PERIODO/2;
    clk <= '0';
    wait for PERIODO/2;
    if (stop_sig='1') then
        wait;
    end if;
end process;

--Proceso de generación de entradas
process begin

    --Estado inicial
    d <= '0';
    stop_sig <= '0';

    --Agregar un desfasaje inicial
    wait for PERIODO/3;

    --Usar un bucle que recorre el vector de estimulos (en=0)
	en <= '0';
    test_signals_1 : for i in 0 to (stimulus'length - 1) loop
      d <= stimulus(i);
      wait for PERIODO;
  end loop test_signals_1;

	--Usar un bucle que recorre el vector de estimulos (en=0)
	en <= '1';
	test_signals_2 : for i in 0 to (stimulus'length - 1) loop
	  d <= stimulus(i);
	  wait for PERIODO;
  end loop test_signals_2;

    stop_sig <= '1';
    wait;
end process;

end architecture tb_arq;
