library ieee; 						--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;		--Paquete para std_logic y std_logic_vector

--Entidad
entity FF_D_En is

	port(
		d_in		: in std_logic;
		clk_in		: in std_logic;
		reset_in	: in std_logic;
		enable_in	: in std_logic;
		q_out	: out std_logic);

end entity FF_D_En;

--Arquitectura
architecture FF_D_En_arch of FF_D_En is
begin

	process(clk_in, reset_in)
	begin
		if(reset_in = '1') then
			q_out <= '0';
		elsif(rising_edge(clk_in)) then
			if(enable_in = '1') then
				q_out <= d_in;
			end if;
		end if;
	end process;


end architecture FF_D_En_arch;
