--------------------------------------------------------------------------------
--- Entidad: MUX_Display_Hexa.
--- Descripción: Esta entidad es multiplexor temporal para 4 displays 7 de
--				 segmentos. Se usa un contador interno de nBits, donde los
--				 2 MSB se usan para seleccionar con un MUX que datos de entrada
--				 BCD0_in, BCD1_in, BCD2_in o BCD3_in se decodifican y luego se
--				 inyectan en la salida segData_out, y que display se activa,
--				 decodificando la señal a 4 bits y enviándola a segEnable_out.
--
--				La frecuencia de refresco de cada dígito es igual a:
--						TR = FrecIN / [2 ^(nBits-2)]
--
--				Para variar esta tasa se debe alterar el valor de nBits. Para la
--				EDU-CIAA-FPGA, con FrecIn de 12 MHz, se pone nBits igual a 16
--				para tener una TR de 732 Hz aproximadamente.
--
--				Esta entidad funciona con flanco ascendente de clock y tiene
--				una entrada de reset asincrónica que resetea el contador interno
--				y una entrada de habilitación que permite activar o no el
--				dispositivo o usarlo con una frecuencia de clock distinta a la
--				del sistema.
--
--				Se incorpora un decodificador de BCD a 7 segmentos.
--
--				Basado en el libro de Pong. P. Chu "FPGA Prototyping by VHDL
--				examples", cap 4, ejemplo "Time Multiplexing with Hexadecimal
--				digits", Listing 4.15.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 23/08/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee y
--				  paquete numeric_std para uso de signed y unsigned.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 					--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;		--Paquete para signed y unsigned.

--Entidad
entity MUX_Display_Hexa is
	generic(
		nBits : integer := 16
	);
	port(
		BCD0_in			: in std_logic_vector(3 downto 0);
		BCD1_in			: in std_logic_vector(3 downto 0);
		BCD2_in			: in std_logic_vector(3 downto 0);
		BCD3_in			: in std_logic_vector(3 downto 0);
		clock_in	    : in std_logic;
		reset_in		: in std_logic;
		enable_in		: in std_logic;
		segData_out		: out std_logic_vector(6 downto 0);
		segEnable_out	: out std_logic_vector(3 downto 0));
end entity MUX_Display_Hexa;

--Arquitectura
architecture MUX_Display_Hexa_arch of MUX_Display_Hexa is
	--Señales auxiliares.
	signal Pulses_cnt     : unsigned (nBits-1 downto 0);
	signal segSelection_s : std_logic_vector (1 downto 0);
	signal selectedBCD_s  : std_logic_vector(3 downto 0);

	--Declaración del decodificador BCD a 7 segmentos a usar.
	component BCDTo7seg is
		generic (
			commonAnode : BOOLEAN := TRUE
		);
		port (
			BCD_in   : in  std_logic_vector(3 downto 0);
			ena_in   : in  std_logic;
			seg7_out : out std_logic_vector(6 downto 0)
		);
	end component;

begin
	--Instanciación del decodificador BCD a 7 segmentos.
	BCDTo7seg_0 : BCDTo7seg
		generic map (commonAnode => FALSE)
		port map ( BCD_in   => selectedBCD_s,
				   ena_in   => enable_in,
				   seg7_out => segData_out);

	--Proceso de contado y reset.
	CountAndReset: process(clock_in, reset_in)
	begin
		if(reset_in = '1') then
			Pulses_cnt <= (others => '0');
		elsif(rising_edge(clock_in) and enable_in = '1') then
			Pulses_cnt <= (Pulses_cnt + 1);
		end if;
	end process CountAndReset;

	--Asignación de segSelection_s.
	segSelection_s <= std_logic_vector(Pulses_cnt(nBits-1 downto nBits-2));

	--Multiplexación de datos de entrada y habilitación de display.
	MultiplexingAndSegmentEnable: process(segSelection_s, BCD0_in, BCD1_in,
										  BCD2_in, BCD3_in)
	begin
		case segSelection_s is
			when "00" =>
				selectedBCD_s <= BCD0_in;
	    		segEnable_out <= "0001";
			when "01" =>
				selectedBCD_s <= BCD1_in;
				segEnable_out <= "0010";
			when "10" =>
				selectedBCD_s <= BCD2_in;
				segEnable_out <= "0100";
			when others =>
				selectedBCD_s <= BCD3_in;
				segEnable_out <= "1000";
		end case;
	end process MultiplexingAndSegmentEnable;
end architecture MUX_Display_Hexa_arch;
