--------------------------------------------------------------------------------
--- Entidad: MUX_Display_Hexa.
--- Descripción: Este testbench permite probar el multiplexor temporal de 4
--				 displays de 7 segmentos que tiene internamente un decodificador
--				 BCD hexadecimal e 7 segmentos.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 23/08/2020.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee y
--				  paquete numeric_std para uso de signed y unsigned.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                 --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;  --Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;     --Paquete para signed y unsigned.

--Entidad del testbench.
entity MUX_Display_Hexa_tb is
end entity MUX_Display_Hexa_tb;

--Arquitectura del yestbench
architecture MUX_Display_Hexa_tb_arch of MUX_Display_Hexa_tb is
	--Declaración del MUX temporal a probar.
	component MUX_Display_Hexa is
		generic (
			nBits : integer := 16
		);
		port (
			BCD0_in       : in  std_logic_vector(3 downto 0);
			BCD1_in       : in  std_logic_vector(3 downto 0);
			BCD2_in       : in  std_logic_vector(3 downto 0);
			BCD3_in       : in  std_logic_vector(3 downto 0);
			clock_in      : in  std_logic;
			reset_in      : in  std_logic;
			enable_in     : in  std_logic;
			segData_out   : out std_logic_vector(6 downto 0);
			segEnable_out : out std_logic_vector(3 downto 0)
		);
    end component;

	--Declaración de constantes.
	constant TESTED_NBITS   : integer := 16;
	constant PERIOD 	  	: time 	  := 83 ns; --Frec aprox de 12 MHz.

	--Declaración de estímulos y señal de monitoreo.
	--Entradas al MUX.
	signal test_BCD0_s    	: std_logic_vector(3 downto 0);
	signal test_BCD1_s    	: std_logic_vector(3 downto 0);
	signal test_BCD2_s    	: std_logic_vector(3 downto 0);
	signal test_BCD3_s    	: std_logic_vector(3 downto 0);
	signal test_clock_s     : std_logic;
	signal test_reset_s     : std_logic;
	signal test_enable_s	: std_logic;

	--Salidas al MUX.
	signal test_segData_s 	: std_logic_vector(6 downto 0);
	signal test_segEnable_s : std_logic_vector(3 downto 0);

	--Señal auxiliar para detener la simulación (por defecto es FALSE).
	signal stopSimulation_s : BOOLEAN := FALSE;
begin
	--Instanciación del DUT (Device Under Test).
	MUX_Display_Hexa_0 : MUX_Display_Hexa
		generic map ( nBits => TESTED_NBITS)
		port map ( BCD0_in        => test_BCD0_s,
				   BCD1_in        => test_BCD1_s,
				   BCD2_in        => test_BCD2_s,
				   BCD3_in        => test_BCD3_s,
				   clock_in      => test_clock_s,
				   reset_in      => test_reset_s,
				   enable_in     => test_enable_s,
				   segData_out   => test_segData_s,
				   segEnable_out => test_segEnable_s);

	--Proceso de generación de clock.
	clockGeneration : process
	begin
	    test_clock_s <= '1';
	    wait for PERIOD/2;
	    test_clock_s <= '0';
	    wait for PERIOD/2;
	    if (stopSimulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

	--Proceso de cambio de las entradas hexadecimales.
	applyStimulus : process
	begin
		--Señales de entrada
		test_enable_s <= '1';
		test_BCD0_s <= (others => '0');
		test_BCD1_s <= (others => '0');
		test_BCD2_s <= (others => '0');
		test_BCD3_s <= (others => '0');

		--Reset inicial que dura dos periodos de clock.
		test_reset_s <= '1';
		wait for (2.5)*PERIOD;
		test_reset_s <= '0';

		--Se dejan pasar 16384 para ver que copie bien BCD0_in decodificada.
		for i in 1 to 16 loop
			wait for PERIOD;
			test_BCD0_s <= std_logic_vector(unsigned(test_BCD0_s) + 1);
		end loop;

		wait for PERIOD*16368;

		--Se dejan pasar 16384 para ver que copie bien BCD1_in decodificada.
		for i in 1 to 16 loop
			wait for PERIOD;
			test_BCD1_s <= std_logic_vector(unsigned(test_BCD1_s) + 1);
		end loop;

		wait for PERIOD*16368;

		--Se dejan pasar 16384 para ver que copie bien BCD2_in decodificada.
		for i in 1 to 16 loop
			wait for PERIOD;
			test_BCD2_s <= std_logic_vector(unsigned(test_BCD2_s) + 1);
		end loop;

		wait for PERIOD*16368;

		--Se dejan pasar 16384 para ver que copie bien BCD3_in decodificada.
		for i in 1 to 16 loop
			wait for PERIOD;
			test_BCD3_s <= std_logic_vector(unsigned(test_BCD3_s) + 1);
		end loop;

		wait for PERIOD*16368;

		--Prueba de habilitación.
		test_enable_s <= '0';
		wait for PERIOD*16384;

		--Se detiene la simulación.
		stopSimulation_s <= TRUE;
		wait;
	end process applyStimulus;
end architecture MUX_Display_Hexa_tb_arch;
