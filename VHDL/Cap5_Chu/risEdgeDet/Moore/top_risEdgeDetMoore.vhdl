--------------------------------------------------------------------------------
--- Entidad: top_risEdgeDetMoore.
--- Descripción: Esta top-level permite probar al detector de flanco ascendente
--               risEdgeDetMoore en la EDU-CIAA-FPGA.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 22/02/2021.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 					--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.

--Entidad.
entity top_risEdgeDetMoore is
    port(
        clock_in            : in  std_logic;
        reset_in            : in  std_logic;
        level_in            : in  std_logic;
        tick_out            : out std_logic;
        enableReference_out : out std_logic
    );
end entity top_risEdgeDetMoore;

--Arquitectura
architecture top_risEdgeDetMoore_arch of top_risEdgeDetMoore is
    --Declaración del detector de flanco a probar.
    component risEdgeDetMoore
        port (
            clock_in : in  std_logic;
            reset_in : in  std_logic;
            level_in : in  std_logic;
            tick_out : out std_logic
        );
    end component risEdgeDetMoore;

    --Declaración del contador interno que se usa como prescaler.
    component univCounter is
        generic (
            nBits           : integer := 8;
            modulus         : integer := 256;
            risingEdgeClock : BOOLEAN := TRUE
        );
        port (
            d_in              : in  std_logic_vector(nBits-1 downto 0);
            clock_in          : in  std_logic;
            outEnable_in      : in  std_logic;
            reset_in          : in  std_logic;
            counterEnable_in  : in  std_logic;
            load_in           : in  std_logic;
            countUp_in        : in  std_logic;
            q_out             : out std_logic_vector(nBits-1 downto 0);
            terminalCount_out : out std_logic
        );
    end component;

    --Declaración de constantes para el contador interno.
    constant COUNTER_NBITS       : integer := 24;
    constant COUNTER_MODULUS     : integer := 12000000;
    constant COUNTER_RISING_EDGE : boolean := TRUE;

    --Declaración de señales internas para conexiones del contador interno.
    signal counterInputData_s     : std_logic_vector(COUNTER_NBITS-1 downto 0)
                                    := (others => '0');
    signal counterLoad_ena        : std_logic := '0';
    signal counterUpCount_ena     : std_logic := '1';
    signal counter_clk            : std_logic;
    signal counterClock_ena       : std_logic := '1'; --Contador siempre habilitado.
    signal counterOut_ena         : std_logic := '1'; --Salida del prescaler siempre activa.
    signal counterTerminalCount_s : std_logic;

    --Declaración de señales internas para conexiones del detector interno.
    signal detector_rst      : std_logic;
    signal detectorLevelIn_s : std_logic;
    signal detectorTickOut_s : std_logic;

    --Declaración de señal de 1 bit que cambia cada vez que el detector interno
    --está habilitado y recibe un flanco de clock que activa su funcionamiento.
    signal detectorEnableReference_s : std_logic := '0';
begin
    --Instanciación del detector interno.
    risEdgeDetMoore_0 : risEdgeDetMoore
        port map (
          clock_in => counterTerminalCount_s,
          reset_in => detector_rst,
          level_in => detectorLevelIn_s,
          tick_out => detectorTickOut_s
        );

    --Instanciación del contador interno que se usa como prescaler. La entrada
    --d_in se conecta a 0 por medio de la señal counterInputData_s ya que no
    --la usa. La salida q_out se deja abierta porque no se la utilza tampoco.
    univCounter_0 : univCounter
        generic map ( nBits           => COUNTER_NBITS,
                      modulus         => COUNTER_MODULUS,
                      risingEdgeClock => COUNTER_RISING_EDGE)
        port map ( d_in              => counterInputData_s,
                   clock_in          => counter_clk,
                   outEnable_in      => counterOut_ena,
                   reset_in          => detector_rst,
                   counterEnable_in  => counterClock_ena,
                   load_in           => counterLoad_ena,
                   countUp_in        => counterUpCount_ena,
                   q_out             => open,
                   terminalCount_out => counterTerminalCount_s);

    --Se hacen las conexiones internas.
    counter_clk         <= clock_in;
    detector_rst        <= not reset_in; --Se niega por resistor pull-up.
    detectorLevelIn_s   <= not level_in; --Se niega por resistor pull-up.
    tick_out            <= detectorTickOut_s;
    enableReference_out <= detectorEnableReference_s;

    --Proceso para generar la señal detectorEnableReference_s. Esta se comporta
    --como la salida de un Flip Flop T.
    getEnableReference : process (clock_in)
    begin
        if( (rising_edge(clock_in)) and
            (counterTerminalCount_s = '1')) then
            detectorEnableReference_s <= not detectorEnableReference_s;
        end if;
    end process getEnableReference;
end architecture top_risEdgeDetMoore_arch;
