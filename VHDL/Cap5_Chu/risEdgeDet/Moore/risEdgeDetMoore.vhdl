--------------------------------------------------------------------------------
--- Entidad: risEdgeDetMoore.
--- Descripción: Esta entidad es un detector de flanco ascendente implementado
--               como una FSM con salida Moore.
--- Bibliografía: P.P. Chu, "FPGA prototyping by VHDL examples". New Jersey:
--                John Wiley & Sons, 2008, pp. 114-116.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 22/02/2021.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 					--Biblioteca estándar IEEE.
use ieee.std_logic_1164.all;	--Paquete para std_logic y std_logic_vector.

--Entidad.
entity risEdgeDetMoore is
    port(
        clock_in : in  std_logic;
        reset_in : in  std_logic;
        level_in : in  std_logic;
        tick_out : out std_logic
    );
end entity risEdgeDetMoore;

--Arquitectura
architecture risEdgeDetMoore_arch of risEdgeDetMoore is
    --Tipo de dato para los estados.
    type state_type is (low_state, edge_state, high_state);
    signal regState, nextState : state_type;
begin
    --Lógica de estado siguiente.
    nextStateLogic : process(level_in, regState)
    begin
        case regState is
            when low_state =>
                if(level_in = '1') then
                    nextState <= edge_state;
                else
                    nextState <= low_state;
                end if;
            when edge_state =>
                if(level_in = '1') then
                    nextState <= high_state;
                else
                    nextState <= low_state;
                end if;
            when high_state =>
                if(level_in = '0') then
                    nextState <= low_state;
                else
                    nextState <= high_state;
                end if;
            when others =>
                nextState <= low_state; --En caso de error (estado indeseado).
        end case;
    end process nextStateLogic;

    --Actualización del estado actual.
    updateState : process(clock_in, reset_in)
    begin
        if(reset_in = '1') then
            regState <= low_state;
        elsif(rising_edge(clock_in)) then
            regState <= nextState;
        end if;
    end process updateState;

    --Lógica de la salida Moore.
    outputLogic : process(regState)
    begin
        case regState is
            when low_state | high_state =>
                tick_out <= '0';
            when edge_state =>
                tick_out <= '1';
            when others =>
                tick_out <= '0'; --En caso de error (estado indeseado).
        end case;
    end process outputLogic;
end architecture risEdgeDetMoore_arch;
