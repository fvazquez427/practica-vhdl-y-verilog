--------------------------------------------------------------------------------
--- Entidad: dualEdgeDetMealy_tb.
--- Descripción: Este testbench permite probar al detector de flanco ascendente
--               dualEdgeDetMealy.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 23/02/2021.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.

--Entidad del testbench.
entity dualEdgeDetMealy_tb is
end entity dualEdgeDetMealy_tb;

--Arquitectura del testbench.
architecture dualEdgeDetMealy_tb_arch of dualEdgeDetMealy_tb is
	--Declaración del detector de flanco a probar.
    component dualEdgeDetMealy
        port (
            clock_in : in  std_logic;
            reset_in : in  std_logic;
            level_in : in  std_logic;
            tick_out : out std_logic
        );
    end component dualEdgeDetMealy;

	--Declaración de constantes.
	constant PERIOD : time := 100 ns; --Fclock = 10 MHz.

	--Declaración de estímulos y señal de monitoreo.
	--Entradas al detector.
	signal test_clock_s : std_logic;
	signal test_reset_s : std_logic;
    signal test_level_s : std_logic;

	--Salida al detector.
    signal test_tick_s : std_logic;

	--Señal auxiliar para detener la simulación (por defecto es FALSE).
	signal stopSimulation_s : BOOLEAN := FALSE;
begin
	--Instanciación del DUT.
    dualEdgeDetMealy_0 : dualEdgeDetMealy
        port map (
          clock_in => test_clock_s,
          reset_in => test_reset_s,
          level_in => test_level_s,
          tick_out => test_tick_s
        );

	--Proceso de generación de clock.
	clockGeneration : process
	begin
	    test_clock_s <= '1';
	    wait for PERIOD/2;
	    test_clock_s <= '0';
	    wait for PERIOD/2;
	    if (stopSimulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

	--Proceso de aplicación de estímulos en la entrada del DUT.
	applyStimulus : process
	begin
		--Reset inicial que dura dos periodos de clock.
		test_level_s <= '0';
		test_reset_s <= '1';
		wait for (2.5)*PERIOD;
		test_reset_s <= '0';

		--Se deja pasar un período.
        wait for PERIOD;

        --Se pone en alto test_level_s durante 1 período de clock. Luego se la
        --baja por un período.
		test_level_s <= '1';
		wait for PERIOD;
        test_level_s <= '0';
        wait for PERIOD;

        --Se pone en alto test_level_s durante 4 períodos de clock. Luego se la
        --baja por otros 4 período.
		test_level_s <= '1';
		wait for 4*PERIOD;
        test_level_s <= '0';
        wait for 4*PERIOD;

        --Se repite lo anterior pero reiniciando el detector. La salida
        --test_tick_s permanece en alto por 4 períodos de reloj por el
        --comportamiento de la FSM.
        test_reset_s <= '1';
        test_level_s <= '1';
		wait for 4*PERIOD;
        test_level_s <= '0';
        wait for 4*PERIOD;

		--Se detiene la simulación.
		stopSimulation_s <= TRUE;
		wait;
	end process applyStimulus;
end architecture dualEdgeDetMealy_tb_arch;
