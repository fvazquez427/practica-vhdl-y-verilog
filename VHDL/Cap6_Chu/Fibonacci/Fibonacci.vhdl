--------------------------------------------------------------------------------
--- Entidad: Fibonacci.
--- Descripción: Esta entidad es una calculadora de números Fibonacci
--               implementada como una FSMD.
--- Bibliografía: P.P. Chu, "FPGA prototyping by VHDL examples". New Jersey:
--                John Wiley & Sons, 2008, pp. 140-143.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 25/02/2021.
--- Dependencias: Paquetes std_logic_1164.all y numeric_std.all de la biblioteca
--                estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 				 --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;    --Paquete para signed y unsigned.

--Entidad.
entity Fibonacci is
    generic(
        NUM_BITS : integer := 8;
        RES_BITS : integer := 16
    );
    port(
        clock_in    : in  std_logic;
        reset_in    : in  std_logic;
        num_in      : in  std_logic_vector(NUM_BITS-1 downto 0);
        start_in    : in  std_logic;
        fibNum_out  : out std_logic_vector(RES_BITS-1 downto 0);
        ready_out   : out std_logic;
        running_out : out std_logic;
        done_out    : out std_logic
    );
end entity Fibonacci;

--Arquitectura
architecture Fibonacci_arch of Fibonacci is
    --Señales para registros en el camino de datos.
    signal t0_reg     : unsigned(RES_BITS-1 downto 0);
    signal t0Next_reg : unsigned(RES_BITS-1 downto 0);

    signal t1_reg     : unsigned(RES_BITS-1 downto 0);
    signal t1Next_reg : unsigned(RES_BITS-1 downto 0);

    signal n_reg     : unsigned(NUM_BITS-1 downto 0);
    signal nNext_reg : unsigned(NUM_BITS-1 downto 0);

    --Tipo de dato para los estados de la FSM (camino de control).
    type state_type is (idle_state, op_state, done_state);
    signal regState, nextState : state_type;

    --Constantes para legibilidad del código.
    constant ZERO_RES : unsigned(RES_BITS-1 downto 0) := to_unsigned(0, RES_BITS);
    constant ONE_RES  : unsigned(RES_BITS-1 downto 0) := to_unsigned(1, RES_BITS);

    constant ZERO_NUM : unsigned(NUM_BITS-1 downto 0) := to_unsigned(0, NUM_BITS);
    constant ONE_NUM  : unsigned(NUM_BITS-1 downto 0) := to_unsigned(1, NUM_BITS);
begin
    --Camino de datos, parte secuencial.
    dataPathSequential : process(clock_in, reset_in)
    begin
        if(reset_in = '1') then
            t0_reg <= ZERO_RES;
            t1_reg <= ZERO_RES;
            n_reg  <= ZERO_NUM;
        elsif(rising_edge(clock_in)) then
            t0_reg <= t0Next_reg;
            t1_reg <= t1Next_reg;
            n_reg  <= nNext_reg;
        end if;
    end process dataPathSequential;

    --Camino de datos, parte combinacional (registros).
    dataPathCombinational : process(regState, t0_reg, t1_reg, n_reg, num_in)
    begin
        --Por defecto no cambian los registros.
        t0Next_reg <= t0_reg;
        t1Next_reg <= t1_reg;
        nNext_reg  <= n_reg;

        --Según el estado.
        case regState is
            when idle_state =>
                t0Next_reg <= ZERO_RES;
                t1Next_reg <= ONE_RES;
                nNext_reg  <= unsigned(num_in);
            when op_state =>
                if(n_reg = ZERO_NUM) then
                    t1Next_reg <= ZERO_RES;
                elsif(n_reg /= ONE_NUM) then
                    t0Next_reg <= t1_reg;
                    t1Next_reg <= t1_reg + t0_reg;
                    nNext_reg  <= n_reg - 1;
                end if;
            when done_state =>
                --No se hace nada.
            when others =>
                --Estados indeseados.
                t0Next_reg <= ZERO_RES;
                t1Next_reg <= ZERO_RES;
                nNext_reg  <= ZERO_NUM;
        end case;
    end process dataPathCombinational;

    --Salida de datos "fibNum_out" del camino de datos.
    fibNum_out <= std_logic_vector(t1_reg) when regState = done_state else
                  (others => 'Z');

    --Lógica de estado siguiente de la FSM.
    FSMNextStateLogic : process(start_in, regState, n_reg)
    begin
        case regState is
            when idle_state =>
                if(start_in = '1') then
                    nextState <= op_state;
                else
                    nextState <= idle_state;
                end if;
            when op_state =>
                if(n_reg = ZERO_NUM or n_reg = ONE_NUM) then
                    nextState <= done_state;
                else
                    nextState <= op_state;
                end if;
            when done_state =>
                nextState <= idle_state;
            when others =>
                nextState <= idle_state; --Para estados indeseados.
        end case;
    end process FSMNextStateLogic;

    --Actualización del estado actual de la FSM.
    FSMUpdateState : process(clock_in, reset_in)
    begin
        if(reset_in = '1') then
            regState <= idle_state;
        elsif(rising_edge(clock_in)) then
            regState <= nextState;
        end if;
    end process FSMUpdateState;

    --Lógica de la salida de la FSM.
    FSMOutputLogic : process(regState)
    begin
        --Por defecto.
        ready_out   <= '0';
        running_out <= '0';
        done_out    <= '0';

        --Según el estado.
        case regState is
            when idle_state =>
                ready_out   <= '1';
            when op_state =>
                running_out <= '1';
            when done_state =>
                done_out    <= '1';
        end case;
    end process FSMOutputLogic;
end architecture Fibonacci_arch;
