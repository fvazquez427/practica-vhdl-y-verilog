--------------------------------------------------------------------------------
--- Entidad: Fibonacci_tb.
--- Descripción: Este testbench permite probar la calculadora de números
--               Fibonacci de nombre homónimo.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 25/02/2021.
--- Dependencias: Paquete std_logic_1164.all de la biblioteca estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.

--Entidad del testbench.
entity Fibonacci_tb is
end entity Fibonacci_tb;

--Arquitectura del testbench.
architecture Fibonacci_tb_arch of Fibonacci_tb is
	--Declaración de la calculadora a probar.
    component Fibonacci
        generic (
            NUM_BITS : integer := 8;
            RES_BITS : integer := 16
        );
        port (
            clock_in    : in  std_logic;
            reset_in    : in  std_logic;
            num_in      : in  std_logic_vector(NUM_BITS-1 downto 0);
            start_in    : in  std_logic;
            fibNum_out  : out std_logic_vector(RES_BITS-1 downto 0);
            ready_out   : out std_logic;
            running_out : out std_logic;
            done_out    : out std_logic
        );
    end component Fibonacci;

	--Declaración de constantes.
	constant PERIOD : time    := 100 ns; --Fclock = 10 MHz.
    constant N_BITS : integer := 8;
    constant R_BITS : integer := 16;

	--Declaración de estímulos y señal de monitoreo.
	--Entradas a la calculadora.
	signal test_clock_s : std_logic;
	signal test_reset_s : std_logic;
    signal test_num_s   : std_logic_vector(N_BITS-1 downto 0);
    signal test_start_s : std_logic;

	--Salidas de la calculadora.
    signal test_fibNum_s  : std_logic_vector(R_BITS-1 downto 0);
    signal test_ready_s   : std_logic;
    signal test_running_s : std_logic;
    signal test_done_s    : std_logic;

	--Señal auxiliar para detener la simulación (por defecto es FALSE).
	signal stopSimulation_s : BOOLEAN := FALSE;
begin
	--Instanciación del DUT.
    Fibonacci_0 : Fibonacci
        generic map (
            NUM_BITS => N_BITS,
            RES_BITS => R_BITS
        )
        port map (
            clock_in    => test_clock_s,
            reset_in    => test_reset_s,
            num_in      => test_num_s,
            start_in    => test_start_s,
            fibNum_out  => test_fibNum_s,
            ready_out   => test_ready_s,
            running_out => test_running_s,
            done_out    => test_done_s
        );

	--Proceso de generación de clock.
	clockGeneration : process
	begin
	    test_clock_s <= '1';
	    wait for PERIOD/2;
	    test_clock_s <= '0';
	    wait for PERIOD/2;
	    if (stopSimulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

	--Proceso de aplicación de estímulos en la entrada del DUT.
	applyStimulus : process
	begin
		--Reset inicial que dura dos periodos de clock.
		test_num_s   <= (others => '0');
        test_start_s <= '0';
		test_reset_s <= '1';
		wait for (2.5)*PERIOD;
		test_reset_s <= '0';

		--Se deja pasar un período.
        wait for PERIOD;

        --Se calcula fib(0)
        test_start_s <= '1';
        wait for PERIOD;
        test_start_s <= '0';
        wait for 2*PERIOD;

        --Se calcula fib(1). Se deja start en 1 para verificar que no compromete
        --el cálculo.
        test_num_s   <= x"01";
        test_start_s <= '1';
        wait for 3*PERIOD;

        --Se calcula fib(2)
        test_num_s   <= x"02";
        wait for PERIOD;
        test_start_s <= '0';
        wait for 3*PERIOD;

        --Se calcula fib(10)
        test_num_s   <= x"0A";
        test_start_s <= '1';
        wait for PERIOD;
        test_start_s <= '0';
        wait for 11*PERIOD;

        --Se calcula fib(24)
        test_num_s   <= x"18";
        test_start_s <= '1';
        wait for PERIOD;
        test_start_s <= '0';
        wait for 25*PERIOD;

		--Se detiene la simulación.
		stopSimulation_s <= TRUE;
		wait;
	end process applyStimulus;
end architecture Fibonacci_tb_arch;
