--------------------------------------------------------------------------------
--- Entidad: periodCounter.
--- Descripción: Esta entidad es un contador de períodos implementado como una
--               FSMD.
--- Bibliografía: P.P. Chu, "FPGA prototyping by VHDL examples". New Jersey:
--                John Wiley & Sons, 2008, pp. 150-153.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 25/02/2021.
--- Dependencias: Paquetes std_logic_1164.all y numeric_std.all de la biblioteca
--                estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee; 				 --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;    --Paquete para signed y unsigned.

--Entidad.
entity periodCounter is
    generic(
        RES_BITS : integer := 20 --Para llegar a 1 Hz de mínima frecuencia.
    );
    port(
        clock_in    : in  std_logic;
        reset_in    : in  std_logic;
        start_in    : in  std_logic;
        sig_in      : in  std_logic;
        period_out  : out std_logic_vector(RES_BITS-1 downto 0);
        ready_out   : out std_logic;
        running_out : out std_logic;
        done_out    : out std_logic
    );
end entity periodCounter;

--Arquitectura
architecture periodCounter_arch of periodCounter is
    --Declaración del detector de flanco interno.
    component risEdgeDetMealy
        port (
            clock_in : in  std_logic;
            reset_in : in  std_logic;
            level_in : in  std_logic;
            tick_out : out std_logic
        );
    end component risEdgeDetMealy;

    --Constantes para legibilidad del código.
    constant COUNTER_BITS : integer := 4;
    constant COUNTER_CMAX : unsigned(COUNTER_BITS-1 downto 0) :=
        to_unsigned(11, COUNTER_BITS);

    --Señales para contador y registros del camino de datos.
    --Contador de períodos de clock.
    signal per_cnt     : unsigned(COUNTER_BITS-1 downto 0);
    signal perNext_cnt : unsigned(COUNTER_BITS-1 downto 0);

    --Registro de microsegundos o resolución.
    signal res_reg     : unsigned(RES_BITS-1 downto 0);
    signal resNext_reg : unsigned(RES_BITS-1 downto 0);

    --Registro de salida.
    signal out_reg     : unsigned(RES_BITS-1 downto 0);
    signal outNext_reg : unsigned(RES_BITS-1 downto 0);

    --Señales para conexiones internas del detector de flanco.
    signal det_clk      : std_logic;
    signal det_rst      : std_logic;
    signal detLevelIn_s : std_logic;
    signal detTickOut_s : std_logic;

    --Tipo de dato para los estados de la FSM (camino de control).
    type state_type is (idle_state, wait_state, count_state, done_state);
    signal regState, nextState : state_type;
begin
    --Instanciación del detector de flanco interno.
    risEdgeDetMealy_0 : risEdgeDetMealy
        port map (
            clock_in => det_clk,
            reset_in => det_rst,
            level_in => detLevelIn_s,
            tick_out => detTickOut_s
        );

     --Conexiones del detector de flanco.
     det_clk      <= clock_in;
     det_rst      <= reset_in;
     detLevelIn_s <= sig_in;

    --Camino de datos, parte secuencial.
    dataPathSequential : process(clock_in, reset_in)
    begin
        if(reset_in = '1') then
            per_cnt <= (others => '0');
            res_reg <= (others => '0');
            out_reg <= (others => '0');
        elsif(rising_edge(clock_in)) then
            per_cnt <= perNext_cnt;
            res_reg <= resNext_reg;
            out_reg <= outNext_reg;
        end if;
    end process dataPathSequential;

    --Camino de datos, parte combinacional.
    dataPathCombinational : process(regState, per_cnt, res_reg, out_reg, detTickOut_s)
    begin
        --Por defecto no cambia ni el contador ni el registro.
        perNext_cnt <= per_cnt;
        resNext_reg <= res_reg;
        outNext_reg <= out_reg;

        --Según el estado.
        case regState is
            when idle_state =>
                --No se hace nada.
            when wait_state =>
                if(detTickOut_s = '1') then
                    perNext_cnt <= (others => '0');
                    resNext_reg <= (others => '0');
                end if;
            when count_state =>
                if(detTickOut_s /= '1') then
                    if(per_cnt = COUNTER_CMAX) then
                        perNext_cnt <= (others => '0');
                        resNext_reg <= res_reg + 1;
                    else
                        perNext_cnt <= per_cnt + 1;
                    end if;
                else
                    outNext_reg <= res_reg;
                end if;
            when done_state =>
                --No se hace nada.
            when others =>
                --Estados indeseados.
                perNext_cnt <= (others => '0');
                resNext_reg <= (others => '0');
                outNext_reg <= (others => '0');
        end case;
    end process dataPathCombinational;

    --Salida de datos "period_out" del camino de datos.
    period_out <= std_logic_vector(out_reg);

    --Lógica de estado siguiente de la FSM.
    FSMNextStateLogic : process(start_in, regState, detTickOut_s)
    begin
        case regState is
            when idle_state =>
                if(start_in = '1') then
                    nextState <= wait_state;
                else
                    nextState <= idle_state;
                end if;
            when wait_state =>
                if(detTickOut_s = '1') then
                    nextState <= count_state;
                else
                    nextState <= wait_state;
                end if;
            when count_state =>
                if(detTickOut_s = '1') then
                    nextState <= done_state;
                else
                    nextState <= count_state;
                end if;
            when done_state =>
                nextState <= idle_state;
            when others =>
                nextState <= idle_state; --Para estados indeseados.
        end case;
    end process FSMNextStateLogic;

    --Actualización del estado actual de la FSM.
    FSMUpdateState : process(clock_in, reset_in)
    begin
        if(reset_in = '1') then
            regState <= idle_state;
        elsif(rising_edge(clock_in)) then
            regState <= nextState;
        end if;
    end process FSMUpdateState;

    --Lógica de la salida de la FSM.
    FSMOutputLogic : process(regState)
    begin
        --Por defecto.
        ready_out   <= '0';
        running_out <= '0';
        done_out    <= '0';

        --Según el estado.
        case regState is
            when idle_state =>
                ready_out   <= '1';
            when wait_state | count_state =>
                running_out <= '1';
            when done_state =>
                done_out    <= '1';
        end case;
    end process FSMOutputLogic;
end architecture periodCounter_arch;
