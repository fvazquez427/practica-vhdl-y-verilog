--------------------------------------------------------------------------------
--- Entidad: periodCounter_tb.
--- Descripción: Este testbench permite probar el contador de períodos
--               periodCounter.
--- Autor: Federico Alejandro Vazquez Saraullo.
--- Ultima revisión: 25/02/2021.
--- Dependencias: Paquetes std_logic_1164.all y numeric_std.all de la biblioteca
--                estándar ieee.
--------------------------------------------------------------------------------
--Inclusión de paquetes.
library ieee;                --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.
use ieee.numeric_std.all;    --Paquete para signed y unsigned.

--Entidad del testbench.
entity periodCounter_tb is
end entity periodCounter_tb;

--Arquitectura del testbench.
architecture periodCounter_tb_arch of periodCounter_tb is
	--Declaración del contador de períodos a probar.
    component periodCounter
        generic (
            RES_BITS : integer := 20
        );
        port (
            clock_in    : in  std_logic;
            reset_in    : in  std_logic;
            start_in    : in  std_logic;
            sig_in      : in  std_logic;
            period_out  : out std_logic_vector(RES_BITS-1 downto 0);
            ready_out   : out std_logic;
            running_out : out std_logic;
            done_out    : out std_logic
        );
    end component periodCounter;

	--Declaración de constantes.
	constant CLOCK_PERIOD         : time := 83.3333 ns;  --Fclock = 12 MHz.
    constant INPUT_SIGNAL_PERIOD1 : time := 1 us;        --Finput = 1 MHz.
    constant INPUT_SIGNAL_PERIOD2 : time := 10 us;       --Finput = 100 kHz.
    constant INPUT_SIGNAL_PERIOD3 : time := 100 us;      --Finput = 10 kHz.
    constant INPUT_SIGNAL_PERIOD4 : time := 1 ms;        --Finput = 1 kHz.
    constant INPUT_SIGNAL_PERIOD5 : time := 10 ms;       --Finput = 100 Hz.
    constant INPUT_SIGNAL_PERIOD6 : time := 100 ms;      --Finput = 10 Hz.

    constant R_BITS               : integer := 20;

	--Declaración de estímulos y señal de monitoreo.
	--Entradas al contador de períodos.
	signal test_clock_s : std_logic;
	signal test_reset_s : std_logic;
    signal test_start_s : std_logic;
    signal test_sig_s   : std_logic;

	--Salidas del contador de períodos.
    signal test_period_s  : std_logic_vector(R_BITS-1 downto 0);
    signal test_ready_s   : std_logic;
    signal test_running_s : std_logic;
    signal test_done_s    : std_logic;

	--Señal auxiliar para detener la simulación (por defecto es FALSE).
	signal stopSimulation_s : BOOLEAN := FALSE;

    --Tipo de dato para elegir la señal de entrada.
    type inputSig_type is (sig1, sig2, sig3, sig4, sig5, sig6);
    signal inputSig : inputSig_type;
begin
	--Instanciación del DUT.
    periodCounter_0 : periodCounter
        generic map (
            RES_BITS => R_BITS
        )
        port map (
            clock_in    => test_clock_s,
            reset_in    => test_reset_s,
            start_in    => test_start_s,
            sig_in      => test_sig_s,
            period_out  => test_period_s,
            ready_out   => test_ready_s,
            running_out => test_running_s,
            done_out    => test_done_s
        );

	--Proceso de generación de clock.
	clockGeneration : process
	begin
	    test_clock_s <= '1';
	    wait for CLOCK_PERIOD/2;
	    test_clock_s <= '0';
	    wait for CLOCK_PERIOD/2;
	    if (stopSimulation_s = TRUE) then
	        wait;
	    end if;
	end process clockGeneration;

    inputSignalGeneration : process
    begin
        test_sig_s <= '0';

        case inputSig is
            when sig1 =>
                wait for INPUT_SIGNAL_PERIOD1/2;
            when sig2 =>
                wait for INPUT_SIGNAL_PERIOD2/2;
            when sig3 =>
                wait for INPUT_SIGNAL_PERIOD3/2;
            when sig4 =>
                wait for INPUT_SIGNAL_PERIOD4/2;
            when sig5 =>
                wait for INPUT_SIGNAL_PERIOD5/2;
            when sig6 =>
                wait for INPUT_SIGNAL_PERIOD6/2;
        end case;

        test_sig_s <= '1';

        case inputSig is
            when sig1 =>
                wait for INPUT_SIGNAL_PERIOD1/2;
            when sig2 =>
                wait for INPUT_SIGNAL_PERIOD2/2;
            when sig3 =>
                wait for INPUT_SIGNAL_PERIOD3/2;
            when sig4 =>
                wait for INPUT_SIGNAL_PERIOD4/2;
            when sig5 =>
                wait for INPUT_SIGNAL_PERIOD5/2;
            when sig6 =>
                wait for INPUT_SIGNAL_PERIOD6/2;
        end case;

        if (stopSimulation_s = TRUE) then
            wait;
        end if;
    end process inputSignalGeneration;

	--Proceso de aplicación de estímulos en la entrada del DUT.
	applyStimulus : process
	begin
		--Reset inicial que dura dos periodos de clock.
        inputSig <= sig1;
        test_start_s <= '0';
		test_reset_s <= '1';
		wait for (2.5)*CLOCK_PERIOD;
		test_reset_s <= '0';

		--Se deja pasar un período.
        wait for CLOCK_PERIOD;

        --Se arranca y espera a que termine el conteo.
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p1 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

        --Se selecciona la señal 2 y se mide nuevamente.
        inputSig <= sig2;
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p2 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

        --Se selecciona la señal 3 y se mide nuevamente.
        inputSig <= sig3;
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p3 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

        --Se selecciona la señal 4 y se mide nuevamente.
        inputSig <= sig4;
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p4 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

        --Se selecciona la señal 5 y se mide nuevamente.
        inputSig <= sig5;
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p5 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

        --Se selecciona la señal 6 y se mide nuevamente.
        inputSig <= sig6;
        test_start_s <= '1';
        wait for CLOCK_PERIOD;
        test_start_s <= '0';
        wait on test_done_s;

        report "p6 = " & integer'image(to_integer(unsigned(test_period_s))) & " us"
        severity note;
        wait for 2*CLOCK_PERIOD;

		--Se detiene la simulación.
		stopSimulation_s <= TRUE;
		wait;
	end process applyStimulus;
end architecture periodCounter_tb_arch;
