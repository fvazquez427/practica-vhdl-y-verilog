library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2_1_8 is
  generic(
  N_BITS : integer := 8
  );
  port (
  a_in : in  std_logic_vector(N_BITS-1 downto 0);
  b_in : in  std_logic_vector(N_BITS-1 downto 0);
  SEL  : in  std_logic;
  z_out: out std_logic_vector(N_BITS-1 downto 0)
  );
end entity;

architecture mux2_1_8ARCH of mux2_1_8 is

  signal mux_sel_s : std_logic ;

begin

  mux_sel_s <= SEL;
  with mux_sel_s select z_out <=
      a_in when '0',
      b_in when others;

end architecture;
