library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU is
  generic(
    N_BITS : integer := 8
  );
  port (
  primer_sumando  : in std_logic_vector(N_BITS-1 downto 0);
  segundo_sumando : in std_logic_vector(N_BITS-1 downto 0);
  s0              : in std_logic;
  s1              : in std_logic;
  s2              : in std_logic;
  s3              : in std_logic;
  s4              : in std_logic;
  carry_out       : out std_logic;
  z_out           : out std_logic_vector(N_BITS-1 downto 0)
  );
end ALU;

architecture ALU_arch of ALU is
  component bitwise_inv is
    port(
    a_in : in std_logic_vector(N_BITS - 1 downto 0);
    EN   : in std_logic ;
    z_out: out std_logic_vector(N_BITS - 1 downto 0)
    );
  end component;
  component mux4_1_8 is
    port(
    a_in : in  std_logic_vector(N_BITS-1 downto 0);
    b_in : in  std_logic_vector(N_BITS-1 downto 0);
    c_in : in  std_logic_vector(N_BITS-1 downto 0);
    d_in : in  std_logic_vector(N_BITS-1 downto 0);
    SEL1 : in  std_logic;
    SEl2 : in  std_logic;
    z_out: out std_logic_vector(N_BITS-1 downto 0)
    );
  end component;
  component ripple_adder_v1 is
    port(
    c_in : in  std_logic;
    c_out: out std_logic;
    a_in : in  std_logic_vector(N_BITS-1 downto 0);
    b_in : in  std_logic_vector(N_BITS-1 downto 0);
    z_out: out std_logic_vector(N_BITS-1 downto 0)
    );
  end component;
  --signal del mux
  signal adder_out_s       : std_logic_vector(N_BITS-1 downto 0);
  signal bitwise_and_out_s : std_logic_vector(N_BITS-1 downto 0);
  signal mux_sel_0         : std_logic;
  signal mux_sel_1         : std_logic;
  signal ALU_out_s          : std_logic_vector(N_BITS-1 downto 0);
  --signal ripple adder
  signal bitwise_and_out_1_s : std_logic_vector(N_BITS-1 downto 0);
  signal primer_sumando_s    : std_logic_vector (N_BITS-1 downto 0);
  signal segundo_sumando_s   : std_logic_vector (N_BITS-1 downto 0);
  signal bitwise_inv_en_s    : std_logic ;
  signal carry_in_s          : std_logic ;
  signal carry_out_s         : std_logic ;
  --signal para la bitwise_and_1
  signal s4_expand_s : std_logic_vector(N_BITS-1 downto 0);
  signal bitwise_inv_out_s : std_logic_vector(N_BITS-1 downto 0);

begin
  bitwise_inv_en_s  <= s3 ;
  carry_in_s        <= s2 ;
  primer_sumando_s  <= primer_sumando  ;
  segundo_sumando_s <= segundo_sumando ;
  mux_sel_0 <= s0 ;
  mux_sel_1 <= s1 ;
  s4_expand_s <= (s4 & s4 & s4 & s4 & s4 & s4 & s4 & s4);
  bitwise_inv_v1 : bitwise_inv port map ( a_in  => segundo_sumando_s,
                                          EN    => bitwise_inv_en_s ,
                                          z_out => bitwise_inv_out_s);
  bitwise_and_out_1_s <= bitwise_inv_out_s and (not s4_expand_s);
  ripple_adder : ripple_adder_v1 port map( a_in  => primer_sumando_s,
                                           b_in  => bitwise_and_out_1_s,
                                           c_in  => carry_in_s,
                                           c_out => carry_out_s,
                                           z_out => adder_out_s);

  bitwise_and_out_s <= primer_sumando_s and segundo_sumando_s ;
  carry_out <= carry_out_s ;
  mux_4_1_8_v1 : mux4_1_8 port map ( a_in  => adder_out_s,
                                      b_in  => bitwise_and_out_s,
                                      c_in  => primer_sumando_s,
                                      d_in  => segundo_sumando_s,
                                      SEL1  => mux_sel_1,
                                      SEL2  => mux_sel_0,
                                      z_out => ALU_out_s
                                    );
 z_out <= ALU_out_s;


end architecture;
