library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity split16 is
  generic ( N_BITS : integer := 16);
  port (
  a_in : in  std_logic_vector ( N_BITS-1 downto 0 );
  high : out std_logic_vector ( (N_BITS/2)-1 downto 0 );
  low  : out std_logic_vector ( (N_BITS/2)-1 downto 0 )
  );
end entity;

architecture split_arch of split16 is
begin
  high <= a_in ( N_BITS-1 downto  (N_BITS/2));
  low  <= a_in ( (N_BITS/2)-1 downto  0);
end architecture;
