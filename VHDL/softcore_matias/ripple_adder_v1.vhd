library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ripple_adder_v1 is
  generic(
    N_BITS : integer := 8
  );
  port (
  c_in : in  std_logic;
  c_out: out std_logic;
  a_in : in  std_logic_vector(N_BITS-1 downto 0);
  b_in : in  std_logic_vector(N_BITS-1 downto 0);
  z_out: out std_logic_vector(N_BITS-1 downto 0)
  );
end ripple_adder_v1;

architecture ripple_adder_v1ARCH of ripple_adder_v1 is
  --las signal tienen N bits en vez de N-1 para almacenar el carry
  signal a_in_s : unsigned(N_BITS downto 0);
  signal b_in_s : unsigned(N_BITS downto 0);
  signal c_in_s : unsigned(N_BITS downto 0);
  signal sum_s  : unsigned(N_BITS downto 0);
  constant ZERO : std_logic_vector(N_BITS-1 downto 0) := (others => '0');
begin
  a_in_s  <= unsigned('0' & a_in);
  b_in_s  <= unsigned('0' & b_in);
  c_in_s  <= unsigned(ZERO(N_BITS-1 downto 0) & c_in);
  sum_s   <= a_in_s + b_in_s + c_in_s ;
  z_out <= std_logic_vector(sum_s(N_BITS-1 downto 0)) ;
  c_out   <= sum_s(N_BITS); --extraigo el carry de la ultima posicion

end architecture;
