library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux4_1_8_v1_tb is
end entity;

architecture mux4_1_8_v1_tbARCH of mux4_1_8_v1_tb is

  signal b_in :   std_logic_vector(7 downto 0);
  signal a_in :   std_logic_vector(7 downto 0);
  signal c_in :   std_logic_vector(7 downto 0);
  signal d_in :   std_logic_vector(7 downto 0);
  signal SEL1 :   std_logic;
  signal SEl2 :   std_logic;
  signal z_out:   std_logic_vector(7 downto 0);

  component mux4_1_8 is
    generic(
      N_BITS : integer := 8
    );
     port (
           a_in : in  std_logic_vector(N_BITS-1 downto 0);
           b_in : in  std_logic_vector(N_BITS-1 downto 0);
           c_in : in  std_logic_vector(N_BITS-1 downto 0);
           d_in : in  std_logic_vector(N_BITS-1 downto 0);
           SEL1 : in  std_logic;
           SEl2 : in  std_logic;
           z_out: out std_logic_vector(N_BITS-1 downto 0)
           );
   end component mux4_1_8 ;
begin

  mux : mux4_1_8 port map (a_in  => a_in,
                           b_in  => b_in,
                           c_in  => c_in,
                           d_in  => d_in,
                           SEL1  => SEL1,
                           SEl2  => SEL2,
                           z_out => z_out);
  testProcess : process
  begin
    a_in <= "00001010";
    b_in <= "00001011";
    c_in <= "00001100";
    d_in <= "00001101";
    SEL1 <= '0';
    SEL2 <= '0';
    wait for 50 ns;
    SEL1 <= '0';
    SEL2 <= '1';
    wait for 50 ns;
    SEL1 <= '1';
    SEL2 <= '0';
    wait for 50 ns;
    SEL1 <= '1';
    SEL2 <= '1';
    wait for 50 ns;
    wait;
  end process;

end architecture;
