library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity instruction_decoder is
  generic( N_BITS : integer := 8 );
  port (
  a_in     : in  std_logic_vector(N_BITS-1 downto 0);
  add      : out std_logic ;
  load     : out std_logic ;
  out_put : out std_logic ;
  in_put  : out std_logic ;
  jumpz    : out std_logic ;
  jump     : out std_logic ;
  jumpnz   : out std_logic ;
  jumpc    : out std_logic ;
  jumpnc   : out std_logic ;
  sub      : out std_logic ;
  bitand   : out std_logic
  );
end entity;

architecture instruction_decoder_arch of instruction_decoder is

  signal jump_condition : std_logic ;

begin
  in_put  <=  a_in(7) and (not a_in(6)) and a_in(5) and (not a_in(4))    ;
  out_put <=  a_in(7) and a_in(6) and a_in(5) and (not a_in(4)) ;
  load <= (not a_in(7)) and (not a_in(6)) and (not a_in(5)) and (not a_in(4));
  add <= (not a_in(7)) and  a_in(6) and (not a_in(5)) and (not a_in(4));
  jump <=  a_in(7) and (not a_in(6)) and (not a_in(5)) and (not a_in(4));
  jump_condition <=  a_in(7) and (not a_in(6)) and (not a_in(5)) and a_in(4);
  sub <= (not a_in(7)) and  a_in(6) and  a_in(5) and (not a_in(4));
  bitand <= (not a_in(7)) and (not a_in(6)) and (not a_in(5)) and a_in(4);
  jumpz <= jump_condition and (not a_in (3)) and (not a_in(2)) ;
  jumpnz <= jump_condition and (not a_in (3)) and  a_in(2) ;
  jumpc  <= jump_condition and a_in (3) and (not a_in(2));
  jumpnc <= jump_condition and  a_in (3) and a_in(2) ;

end architecture;
