library ieee;                --Biblioteca estándar IEEE.
use ieee.std_logic_1164.all; --Paquete para std_logic y std_logic_vector.

--Entidad del testbench.
entity ALU_tb is
end entity ALU_tb;

--Arquitectura del testbench.
architecture ALU_tb_arch of ALU_tb is
	--Declaración de la ALU a probar.
    component ALU
        generic (
            N_BITS : integer := 8
        );
        port (
            primer_sumando     : in  std_logic_vector(N_BITS-1 downto 0);
            segundo_sumando    : in  std_logic_vector(N_BITS-1 downto 0);
            s0      : in  std_logic;
            s1      : in  std_logic;
            s2      : in  std_logic;
            s3      : in  std_logic;
            s4      : in  std_logic;
            z_out     : out std_logic_vector(N_BITS-1 downto 0);
            carry_out : out std_logic
        );
    end component ALU;

	--Declaración de constantes.
	constant DELAY    : time    := 100 ns;
    constant NUM_BITS : integer := 8;

    constant A1 : std_logic_vector := x"E8";
    constant B1 : std_logic_vector := x"22";

	--Declaración de estímulos y señal de monitoreo.
	--Entradas a la ALU.
    signal test_a_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_b_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_s0_s  : std_logic;
    signal test_s1_s  : std_logic;
    signal test_s2_s  : std_logic;
    signal test_s3_s  : std_logic;
    signal test_s4_s  : std_logic;

	--Salidas de la ALU.
    signal test_z_s   : std_logic_vector(NUM_BITS-1 downto 0);
    signal test_Car_s : std_logic;

begin
	--Instanciación del DUT.
    ALU_0 : ALU
        generic map (
            N_BITS => NUM_BITS
        )
        port map (
            primer_sumando    => test_a_s,
            segundo_sumando    => test_b_s,
            s0   => test_s0_s,
            s1   => test_s1_s,
            s2   => test_s2_s,
            s3   => test_s3_s,
            s4   => test_s4_s,
            z_out   => test_z_s,
            carry_out => test_Car_s
        );

	--Proceso de aplicación de estímulos en la entrada del DUT.
	applyStimulus : process
	begin
		--Salida: b_in.
        test_s0_s <= '1';
        test_s1_s <= '1';
        test_s2_s <= '0';
        test_s3_s <= '0';
        test_s4_s <= '0';
        test_a_s  <= A1;
        test_b_s  <= B1;
		wait for DELAY;

        --Salida: a_in.
        test_s0_s <= '0';
        wait for DELAY;

        --Salida: a_in and b_in.
        test_s0_s <= '1';
        test_s1_s <= '0';
        wait for DELAY;

        --Salida: a_in + b_in + Cin (0).
        test_s0_s <= '0';
        wait for DELAY;

        --Salida: a_in + b_in + Cin (1).
        test_s2_s <= '1';
        wait for DELAY;

        --Salida: a_in + Cin (1).
        test_s4_s <= '1';
        wait for DELAY;

        --Salida: a_in - b_in (con Cin = 1).
        test_s4_s <= '0';
        test_s3_s <= '1';
        wait for DELAY;

		--Se detiene la simulación.
		wait;
	end process applyStimulus;
end architecture ALU_tb_arch;
