library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity sequence_generator_tb is
end entity;

architecture sequence_generator_tb_arch of sequence_generator_tb is

  signal clock     :  std_logic  ;
  signal CE        :  std_logic  ;
  signal clr       :  std_logic  ;
  signal fetch     :  std_logic  ;
  signal decode    :  std_logic  ;
  signal execute   :  std_logic  ;
  signal increment :  std_logic  ;

  component sequence_generator is
    port (
    clock     : in std_logic  ;
    CE        : in std_logic  ;
    clr       : in std_logic  ;
    fetch     : out std_logic ;
    decode    : out std_logic ;
    execute   : out std_logic ;
    increment : out std_logic
    );
  end component;

  constant PERIOD  :  time := 100 ns;
  signal stop_simulation_s : boolean ;

begin

  sequence_generator_0 : sequence_generator port map (clock     => clock  ,
                                                      CE        => CE     ,
                                                      clr       => clr    ,
                                                      fetch     => fetch  ,
                                                      decode    => decode ,
                                                      execute   => execute,
                                                      increment => increment);



  clock_generation : process
  begin
	    clock <= '1';
	    wait for PERIOD/2;
	    clock <= '0';
	    wait for PERIOD/2;
      if (stop_simulation_s = TRUE) then
	        wait;
	    end if;
	end process clock_generation;

  test_process : process
  begin
    CE <= '1' ;
    clr<= '1' ;
    wait for PERIOD*3;
    clr<= '0' ;
    wait for PERIOD*5;
    CE <= '0' ;
    wait for PERIOD*5;
    stop_simulation_s <= TRUE ;
    wait;
  end process;

end architecture;
