library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bitwise_inv_v1_tb is
end entity;

architecture bitwise_inv_tbARCH of bitwise_inv_v1_tb is

  signal a_in :  std_logic_vector(7 downto 0);
  signal EN   :  std_logic ;
  signal z_out:  std_logic_vector(7 downto 0);

  component bitwise_inv is
    generic(
      N_BITS : integer := 8
    );
     port (
     a_in : in std_logic_vector(N_BITS - 1 downto 0);
     EN   : in std_logic ;
     z_out: out std_logic_vector(N_BITS - 1 downto 0)
     );
  end component bitwise_inv;

begin

  bitwise_inv_test : bitwise_inv port map(a_in  => a_in,
                                          EN    => EN  ,
                                          z_out => z_out);
  testProcess : process
  begin
    EN   <= '1' ;
    a_in <= "10001000" ;
    wait for 50 ns ;
    a_in <= "00001000" ;
    wait for 50 ns ;
    EN   <= '0' ;
    wait ;
  end process;


end architecture;
