library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux4_1_8 is
  generic(
  N_BITS : integer := 8
  );
  port (
  a_in : in  std_logic_vector(N_BITS-1 downto 0);
  b_in : in  std_logic_vector(N_BITS-1 downto 0);
  c_in : in  std_logic_vector(N_BITS-1 downto 0);
  d_in : in  std_logic_vector(N_BITS-1 downto 0);
  SEL1 : in  std_logic;
  SEl2 : in  std_logic;
  z_out: out std_logic_vector(N_BITS-1 downto 0)
  );
end entity;

architecture mux4_1_8ARCH of mux4_1_8 is

  signal mux_sel : std_logic_vector(1 downto 0);

begin

  mux_sel <= SEL1 & SEL2 ;

  with mux_sel select z_out <=
      a_in when "00",
      b_in when "01",
      c_in when "10",
      d_in when others;


end architecture;
