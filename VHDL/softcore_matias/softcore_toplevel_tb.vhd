library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity softcore_toplevel_tb is
end entity;

architecture softcore_toplevel_tb_arch of softcore_toplevel_tb is

  signal clock       :  std_logic  ;
  signal NCLR        :  std_logic  ;
  signal serial_out  :  std_logic ;
  constant PERIOD    : time    := 100 ns;
  signal stop_simulation_s : boolean ;

  component softcore_toplevel is
    port (
    clock       : in std_logic  ;
    NCLR        : in std_logic  ;
    serial_out  : out std_logic
    );
  end component;
begin

  softcore_0 : softcore_toplevel
             port map
             (
             clock => clock ,
             NCLR  => NCLR  ,
             serial_out => serial_out
             );
   clock_generation : process
   begin
  	    clock <= '1';
  	    wait for PERIOD/2;
  	    clock <= '0';
  	    wait for PERIOD/2;
       if (stop_simulation_s = TRUE) then
  	        wait;
  	    end if;
	end process clock_generation;

  test_process : process
  begin
    NCLR <= '0' ;
    wait for 1 ns;
    NCLR <= '1' ;
    wait for PERIOD * 20;
    stop_simulation_s <= TRUE;
    wait ;
  end process;

end architecture;
