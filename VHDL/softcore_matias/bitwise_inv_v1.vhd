library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity bitwise_inv is
  generic(
    N_BITS : integer := 8
  );
  port (
    a_in : in std_logic_vector(N_BITS - 1 downto 0);
    EN   : in std_logic ;
    z_out: out std_logic_vector(N_BITS - 1 downto 0)
  );
end bitwise_inv;

architecture bitwise_invARCH of bitwise_inv is
begin
  bitwise_xor : for i in 0 to N_BITS-1 generate
      z_out(i) <= a_in(i) xor EN ;
  end generate;
end architecture;
