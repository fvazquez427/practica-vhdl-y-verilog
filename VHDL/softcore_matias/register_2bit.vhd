library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_2bit is

  port (
  clock : in  std_logic ;
  a_in  : in  std_logic ;
  b_in  : in  std_logic ;
  CLR   : in  std_logic ;
  CE    : in  std_logic ;
  a_out : out std_logic ;
  b_out : out std_logic
  );
end register_2bit;


architecture behavioral of register_2bit is
  signal a_out_s, b_out_s : std_logic ;
begin
  register_process : process(clock, CE)
  begin
    if CLR = '1' then
      a_out_s <= '0';
      b_out_s <= '0';
    elsif CE = '1' then
      if rising_edge(clock) then
        a_out_s <= a_in ;
        b_out_s <= b_in ;
      end if;
    end if;
  end process;

  a_out <= a_out_s ;
  b_out <= b_out_s ;

end architecture;
