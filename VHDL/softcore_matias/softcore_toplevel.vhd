library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity softcore_toplevel is
  port (
  clock : in std_logic       ;
  NCLR  : in std_logic       ;
  serial_out : out std_logic
  );
end entity;

architecture softcore_toplevel_arch of softcore_toplevel is

--******************************************************************************
-- declaraciones de los componentes dentro del Simple CPU V1
component RAM256x16
    generic (
        WORD_LENGTH : integer := 16;
        ADDR_LENGTH : integer := 8
    );
    port (
        clk_in     : in  std_logic;
        writeEn_in : in  std_logic;
        address_in : in  std_logic_vector(ADDR_LENGTH-1 downto 0);
        data_in    : in  std_logic_vector(WORD_LENGTH-1 downto 0);
        data_out   : out std_logic_vector(WORD_LENGTH-1 downto 0)
    );
end component RAM256x16; -- memoria RAM

component ffd is
  port (
        clock  : in  std_logic ;
        d_in   : in  std_logic ;
        reset  : in  std_logic ;
        ce     : in  std_logic ;
        q_out  : out std_logic
  );
end component; -- flip-flop D de para salida serial

component ffd_sin_reset is
  port (
  clock  : in  std_logic ;
  d_in   : in  std_logic ;
  q_out  : out std_logic
  );
end component; -- flip flop D para la generacion del reset sincronico

component mux2_1_8 is
  generic(
    N_BITS : integer := 8
  );
   port (
   a_in : in  std_logic_vector(N_BITS-1 downto 0);
   b_in : in  std_logic_vector(N_BITS-1 downto 0);
   SEL  : in  std_logic;
   z_out: out std_logic_vector(N_BITS-1 downto 0)
  );
end component mux2_1_8 ; -- multiplexor de 2 a 1 con bus de 8 bits  para
                         -- muxa, muxb (selectores de sumandos para la ALU)
                         -- muxc  (selector de direccion para la RAM)

component register_v1 is
  generic(N_BITS : integer := 8);
  port (
  clock : in std_logic ;
  a_in  : in std_logic_vector(N_BITS-1 downto 0);
  CLR   : in std_logic ;
  CE    : in std_logic ;
  q_out : out std_logic_vector(N_BITS-1 downto 0)
  );
end component; -- registro para Program Counter (16 bits)
               -- Instruction Register (8 bits)
               -- acumulador a la salida de la ALU (8 bits)

component split16 is
  generic ( N_BITS : integer := 16);
  port (
  a_in : in  std_logic_vector ( N_BITS-1 downto 0 );
  high : out std_logic_vector ( (N_BITS/2)-1 downto 0 );
  low  : out std_logic_vector ( (N_BITS/2)-1 downto 0 )
  );
end component; -- modulos que dividen una entrada de 16 bits en dos tiras de 8
               -- para dividir el la salida del IR y la salida de la RAM

component decoder is
  generic (N_BITS : integer := 8 );
  port (
  ir     : in  std_logic_vector(N_BITS-1 downto 0);
  carry  : in  std_logic;
  zero   : in  std_logic;
  clock  : in  std_logic;
  ce     : in  std_logic;
  clr    : in  std_logic;
  mux_a  : out std_logic;
  mux_b  : out std_logic;
  mux_c  : out std_logic;
  en_da  : out std_logic;
  en_pc  : out std_logic;
  en_in  : out std_logic;
  ram    : out std_logic;
  alu_s0 : out std_logic;
  alu_s1 : out std_logic;
  alu_s2 : out std_logic;
  alu_s3 : out std_logic;
  alu_s4 : out std_logic
  );
end component; -- decoder para generar todas las lineas de control

component ALU
    generic (
        N_BITS : integer := 8
    );
    port (
        primer_sumando     : in  std_logic_vector(N_BITS-1 downto 0);
        segundo_sumando    : in  std_logic_vector(N_BITS-1 downto 0);
        s0      : in  std_logic;
        s1      : in  std_logic;
        s2      : in  std_logic;
        s3      : in  std_logic;
        s4      : in  std_logic;
        z_out     : out std_logic_vector(N_BITS-1 downto 0);
        carry_out : out std_logic
    );
end component ALU; -- Unidad aritmética lógica

--******************************************************************************
--declaracion de las señales internas al softcore segun el modelo Simple CPU V1
constant NUM_BITS : integer := 8  ;
constant RAM_WORD : integer := 16 ;
signal clear_s    : std_logic ; -- clear de todo el sistema
signal vcc_s      : std_logic ; -- ce del decoder
signal cpu_di_s : std_logic_vector (RAM_WORD-1 downto 0); --la salida de la ram
signal cpu_do_s : std_logic_vector (RAM_WORD-1 downto 0); --la entrada a la ram
signal ram_we_s : std_logic ; -- habilitacion de escritura de la ram
signal ram_ad_s : std_logic_vector (NUM_BITS-1 downto 0); --entrada de la direccion de la ram, en caso de estar WE en alto esta provee la direccion donde escribir y sino de donde sale el dato
signal clock_s  : std_logic ; -- clock de todos los componentes secuenciales
signal nclock_s : std_logic ; -- clock de la ram ya que esta va negado
signal acc_out_s: std_logic_vector (NUM_BITS-1 downto 0); --la salida del acumulador
signal en_da_s  : std_logic ; -- habilita el clock del acumulador
signal alu_out_s: std_logic_vector (NUM_BITS-1 downto 0); --el result--******************************************************************************ado de la ALU
signal zero_s   : std_logic ; -- condicion de zero generada desde el resultado de la ALU
signal carry_s  : std_logic ; -- condicion de carry generada desde la ALU
signal alu_a_s  : std_logic_vector (NUM_BITS-1 downto 0); --primer sumando de la ALU
signal alu_b_s  : std_logic_vector (NUM_BITS-1 downto 0); --segundo sumando de la ALU
--inicio lineas de control de la ALU
signal alu_s0_s : std_logic ;
signal alu_s1_s : std_logic ;
signal alu_s2_s : std_logic ;
signal alu_s3_s : std_logic ;
signal alu_s4_s : std_logic ;
--fin lineas de control de la ALU
signal pc_out_s : std_logic_vector (NUM_BITS-1 downto 0); -- salida del Program Counter
signal muxa_s   : std_logic ; -- linea selectora del mux que selecciona el primer sumando de la ALU
signal cpu_di_low_s : std_logic_vector (NUM_BITS-1 downto 0); -- los 8 bits menos significativos de la salida de la RAM
signal ir_out_high_s: std_logic_vector (NUM_BITS-1 downto 0); -- los 8 bits mas significativos que salen de la IR
signal muxb_s   : std_logic ; -- linea selectora del mux que selecciona el segundo sumando de la ALU
signal muxc_s   : std_logic ; -- linea selectora del mux que selecciona el valor de la entrada addres de la ram
signal en_pc_s  : std_logic ; -- linea de habilitacion del clock del program counter
signal en_in_s  : std_logic ; -- linea de habilitacion del clock del instrution register
signal ir_out_low_s :std_logic_vector (NUM_BITS-1 downto 0); -- los 8 bits menos significativos de la salida del Instruction register
signal ir_out_s : std_logic_vector(RAM_WORD-1 downto 0); -- la tira completa de bits que salen del ir
signal serial_in_s : std_logic ; -- entrada al flip flop de la salida serial
signal serial_ce_s : std_logic ; -- clock enable del flip flop puesto para la salida serial
signal nserial_out_s:std_logic ; -- salida sin negar del flip flop de la salida serial
signal fd_in  : std_logic ; -- entrada del flip flop FD de generacion de clear
--******************************************************************************
--procedemos a realizar los port map de cada uno de nuestros componentes
begin
  clock_s <= clock      ;
  nclock_s <= not clock ;
  fd_in  <= not NCLR    ;
  --***************************************************************************
  -- Flip Flop de la salida serial y FLip Flop de reset sincronico
  serial_ce_s <= ram_ad_s (0) and ram_ad_s (1) and ram_ad_s (2) and
                 ram_ad_s (3) and ram_ad_s (4) and ram_ad_s (5) and
                 ram_ad_s (6) and ram_ad_s (7) ; -- compuerta and que genera el ce del flip flop de la salida serial
  serial_in_s <= cpu_do_s(0) ;
  FDCE : ffd
        port map (clock => clock_s,
                  d_in  => serial_in_s,
                  ce    => serial_ce_s,
                  reset => clear_s,
                  q_out => nserial_out_s
                  );
  serial_out <= not nserial_out_s ;

  FD : ffd_sin_reset
  port map (clock => clock_s,
            d_in  => fd_in  ,
            q_out => clear_s
            );
  -- fin de los ff
  --***************************************************************************
  -- registros PC (16 bits) ACC e IR (8 bits)
  instruction_register : register_v1
                       generic map (N_BITS => RAM_WORD)
                       port    map (clock => clock_s  ,
                                    a_in  => cpu_di_s ,
                                    CLR   => clear_s  ,
                                    CE    => en_in_s  ,
                                    q_out => ir_out_s);
  program_counter : register_v1
                  generic map (N_BITS => NUM_BITS)
                  port    map (clock  => clock_s  ,
                               a_in   => alu_out_s,
                               CLR    => clear_s  ,
                               CE     => en_pc_s  ,
                               q_out  => pc_out_s);
  acumulador      : register_v1
                  generic map (N_BITS => NUM_BITS)
                  port    map (clock => clock_s  ,
                               a_in  => alu_out_s,
                               CLR   => clear_s  ,
                               CE    => en_da_s  ,
                               q_out => acc_out_s);
  -- fin de los registros
  --***************************************************************************
  -- inicio de los split
  split_del_ir : split16
               port map (a_in => ir_out_s,
                         high => ir_out_high_s,
                         low  => ir_out_low_s );
  split_ram   : split16
               port map (a_in => cpu_di_s,
                         low  => cpu_di_low_s );
  -- fin de los split
  --***************************************************************************
  -- inicio de los multiplexores
  mux2_1_8_a : mux2_1_8
              port map (a_in => acc_out_s,
                        b_in => pc_out_s ,
                        SEL  => muxa_s   ,
                        z_out=> alu_a_s ); -- MUX DEL PRIMER SUMANDO DE LA ALU
  mux2_1_8_b : mux2_1_8
              port map (a_in => cpu_di_low_s,
                       b_in => ir_out_low_s,
                       SEL  => muxb_s      ,
                       z_out=> alu_b_s    ); -- MUX DEL SEGUNDO SUMANDO DE LA ALU
  mux2_1_8_c : mux2_1_8
              port map (a_in => pc_out_s    ,
                        b_in => ir_out_low_s,
                        SEL  => muxc_s      ,
                        z_out=> ram_ad_s    ); -- MUX DE LA DIRECCION DE LA RAM
  -- final de los multiplexores
  --***************************************************************************
  -- inicio de la alu
  ALU_0 : ALU
        port map (primer_sumando => alu_a_s,
                  segundo_sumando=> alu_b_s,
                  s0 => alu_s0_s,
                  s1 => alu_s1_s,
                  s2 => alu_s2_s,
                  s3 => alu_s3_s,
                  s4 => alu_s4_s,
                  carry_out => carry_s,
                  z_out => alu_out_s );
  -- fin de la alu
  --***************************************************************************
  --inicio del decoder
  zero_s <= '1' when alu_out_s = "00000000" else
            '0' ;
  vcc_s <= '1' ;
  decoder_0 : decoder
            port map (ir => ir_out_high_s,
                      carry => carry_s   ,
                      zero  => zero_s    ,
                      clock => clock_s   ,
                      ce    => vcc_s     ,
                      clr   => clear_s   ,
                      mux_a => muxa_s    ,
                      mux_b => muxb_s    ,
                      mux_c => muxc_s    ,
                      en_da => en_da_s   ,
                      en_pc => en_pc_s   ,
                      en_in => en_in_s   ,
                      ram   => ram_we_s  ,
                      alu_s0 => alu_s0_s ,
                      alu_s1 => alu_s1_s ,
                      alu_s2 => alu_s2_s ,
                      alu_s3 => alu_s3_s ,
                      alu_s4 => alu_s4_s);
  --fin del decoder
  --***************************************************************************
  --inicio de la ram
  cpu_do_s <= x"00" & acc_out_s ;
  ram : RAM256x16 port map (clk_in     => nclock_s ,
                            writeEn_in => ram_we_s ,
                            data_in    => cpu_do_s ,
                            address_in => ram_ad_s ,
                            data_out   => cpu_di_s
                            );
  --fin de la ram
  --***************************************************************************
end architecture;
