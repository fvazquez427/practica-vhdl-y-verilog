library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ffd is
  port (
  clock  : in  std_logic ;
  d_in   : in  std_logic ;
  reset  : in  std_logic ;  -- fuerza el estado actual a ser 0
  q_out  : out std_logic ;
  ce     : in  std_logic
  );
end ffd;

architecture  ffdarch of ffd  is
begin
  ffdProcess : process(clock)
  begin
  if reset = '1' then
    q_out <= '0';
  end if ;
    if ce = '1' then
      if (rising_edge (clock)) then
        if d_in = '1' then
          q_out <= '1';
        elsif d_in = '0' then
          q_out <= '0';
        end if;
      end if;
    end if;
  end process;
end architecture;
