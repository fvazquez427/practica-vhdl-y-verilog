library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ffd_tb is
end entity;

architecture ffd_tbarch of ffd_tb  is

    signal clock  :  std_logic ;
    signal d_in   :  std_logic ;
    signal reset  :  std_logic ;  -- fuerza el estado actual a ser 0
    signal q_out  :  std_logic ;
    signal ce     :  std_logic ;
    signal stop_simulation_s : BOOLEAN := FALSE ;
    constant PERIOD          : time  := 100 ns ;
  component ffd is
    port (
          clock  : in  std_logic ;
          d_in   : in  std_logic ;
          reset  : in  std_logic ;  -- fuerza el estado actual a ser 0
          q_out  : out std_logic ;
          ce     : in  std_logic
    );
  end component;

begin
ffd_prueba: ffd port map (clock => clock , d_in => d_in, reset => reset,
                      q_out => q_out, ce => ce );
clockGeneration : process
  begin
    clock <= '1'      ;
    wait for PERIOD/2 ;
    clock <= '0'      ;
    wait for PERIOD/2 ;
    if stop_simulation_s then
      wait;
    end if;
  end process clockGeneration;

processPrueba : process
  begin
    d_in  <= '1' ;
    reset <= '1' ;
    ce    <= '1' ;
    wait for PERIOD/2 ;
    reset <= '0' ;
    wait for PERIOD/2 ;
    ce <= '0' ;
    wait for PERIOD/2 ;
    ce <= '1' ;
    wait for PERIOD ;
    stop_simulation_s <= TRUE ;
    wait;
  end process;
end architecture;
