library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity register_v1_tb is
end entity;

architecture register_1_tb_arch of register_v1_tb is

  constant NUM_BITS : integer := 8;
  signal clock_s :  std_logic ;
  signal a_in_s  :  std_logic_vector(NUM_BITS-1 downto 0);
  signal CLR_s   :  std_logic ;
  signal CE_s    :  std_logic ;
  signal q_out_s :  std_logic_vector(NUM_BITS-1 downto 0);
  constant PERIOD    : time    := 100 ns;
  signal stop_simulation_s : boolean ;


  component register_v1 is
    generic(N_BITS : integer := 8);
    port (
    clock : in std_logic ;
    a_in  : in std_logic_vector(N_BITS-1 downto 0);
    CLR   : in std_logic ;
    CE    : in std_logic ;
    q_out : out std_logic_vector(N_BITS-1 downto 0)
    );
  end component;

begin

  register_0 : register_v1
          generic map (
              N_BITS => NUM_BITS
          )
          port map(
              clock => clock_s,
              a_in  => a_in_s ,
              CLR   => CLR_s  ,
              CE    => CE_s   ,
              q_out => q_out_s
          );
  clock_generation : process
  begin
	    clock_s <= '1';
	    wait for PERIOD/2;
	    clock_s <= '0';
	    wait for PERIOD/2;
      if (stop_simulation_s = TRUE) then
	        wait;
	    end if;
	end process clock_generation;

  test_process : process
  begin
    CE_s <= '1' ;
    a_in_s <= "00001111" ;
    CLR_s <= '1' ;
    wait for PERIOD/2 ;
    CLR_s <= '0' ;
    wait for PERIOD/2 ;
    a_in_s <= "11110000" ;
    wait for PERIOD/2 ;
    stop_simulation_s <= TRUE ;
    wait ;
  end process;

end architecture;
