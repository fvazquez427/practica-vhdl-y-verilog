library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity  instruction_decoder_tb is
end entity;

architecture instruction_decoder_tb_arch of instruction_decoder_tb is
  signal  a_in     : std_logic_vector (7 downto 0);
  signal  add      :  std_logic ;
  signal  load     :  std_logic ;
  signal  out_put :  std_logic ;
  signal  in_put  :  std_logic ;
  signal  jumpz    :  std_logic ;
  signal  jump     :  std_logic ;
  signal  jumpnz   :  std_logic ;
  signal  jumpc    :  std_logic ;
  signal  jumpnc   :  std_logic ;
  signal  sub      :  std_logic ;
  signal  bitand   :  std_logic ;
  component instruction_decoder is
    generic( N_BITS : integer := 8 );
    port (
    a_in     : in  std_logic_vector(N_BITS-1 downto 0);
    add      : out std_logic ;
    load     : out std_logic ;
    out_put : out std_logic ;
    in_put  : out std_logic ;
    jumpz    : out std_logic ;
    jump     : out std_logic ;
    jumpnz   : out std_logic ;
    jumpc    : out std_logic ;
    jumpnc   : out std_logic ;
    sub      : out std_logic ;
    bitand   : out std_logic
    );
  end component;
begin

  instruction_decoder_0 : instruction_decoder
                          port map (
                          a_in =>  a_in ,
                          add  =>   add ,
                          load =>  load ,
                          out_put => out_put ,
                          in_put  => in_put  ,
                          jumpz    => jumpz    ,
                          jump     => jump     ,
                          jumpnz   => jumpnz   ,
                          jumpc    => jumpc    ,
                          jumpnc   => jumpnc   ,
                          sub      => sub      ,
                          bitand   => bitand
                          );
  testProcess : process
  begin
    a_in <= "00000000" ; --load
    wait for 50 ns ;
    a_in <= "01000000" ; --add
    wait for 50 ns ;
    a_in <= "00010000" ; --and
    wait for 50 ns ;
    a_in <= "01100000" ; --sub
    wait for 50 ns ;
    a_in <= "10100000" ; --input
    wait for 50 ns ;
    a_in <= "11100000" ; --output
    wait for 50 ns ;
    a_in <= "10000000" ; --jump  U
    wait for 50 ns ;
    a_in <= "10010000" ; --jump  Z
    wait for 50 ns ;
    a_in <= "10011000" ; --jump  c
    wait for 50 ns ;
    a_in <= "10010100" ; --jump  NZ
    wait for 50 ns ;
    a_in <= "10011100" ; --jump  NC
    wait for 50 ns ;
    wait;
  end process;

end architecture;
