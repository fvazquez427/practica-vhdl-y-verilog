library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder is
  generic (N_BITS : integer := 8 );
  port (
  ir     : in  std_logic_vector(N_BITS-1 downto 0);
  carry  : in  std_logic;
  zero   : in  std_logic;
  clock  : in  std_logic;
  ce     : in  std_logic;
  clr    : in  std_logic;
  mux_a  : out std_logic;
  mux_b  : out std_logic;
  mux_c  : out std_logic;
  en_da  : out std_logic;
  en_pc  : out std_logic;
  en_in  : out std_logic;
  ram    : out std_logic;
  alu_s0 : out std_logic;
  alu_s1 : out std_logic;
  alu_s2 : out std_logic;
  alu_s3 : out std_logic;
  alu_s4 : out std_logic
  );
end entity;

architecture decoder_toplevel of decoder is

  signal ir_s     :   std_logic_vector(N_BITS-1 downto 0);
  signal ce_s     :   std_logic;
  signal clr_s    :   std_logic;

  signal carry_s, zero_s : std_logic ;
  signal fetch_s, decode_s, execute_s, increment_s : std_logic ; --sequence gen
  signal carry_reg_s, zero_reg_s, en_st_s: std_logic ; --2bit register
  --señales del instruction decoder
  signal add_s    : std_logic ;
  signal load_s   : std_logic ;
  signal output_s : std_logic ;
  signal input_s  : std_logic ;
  signal jump_s   : std_logic ;
  signal jumpz_s  : std_logic ;
  signal jumpc_s  : std_logic ;
  signal jumpnz_s : std_logic ;
  signal jumpnc_s : std_logic ;
  signal sub_s    : std_logic ;
  signal bitand_s : std_logic ;
  signal clock_s  : std_logic ;
  --señales del decoder despues de la and de validacion de estado
  signal add_s_f    : std_logic ;
  signal load_s_f   : std_logic ;
  signal output_s_f : std_logic ;
  signal input_s_f  : std_logic ;
  signal jump_s_f   : std_logic ;
  signal jumpz_s_f  : std_logic ;
  signal jumpc_s_f  : std_logic ;
  signal jumpnz_s_f : std_logic ;
  signal jumpnc_s_f : std_logic ;
  signal sub_s_f    : std_logic ;
  signal bitand_s_f : std_logic ;

  component register_2bit is
    port (
    clock : in  std_logic ;
    a_in  : in  std_logic ;
    b_in  : in  std_logic ;
    CLR   : in  std_logic ;
    CE    : in  std_logic ;
    a_out : out std_logic ;
    b_out : out std_logic
    );
  end component;
  component sequence_generator is
    port (
    clock     : in std_logic  ;
    CE        : in std_logic  ;
    clr       : in std_logic  ;
    fetch     : out std_logic ;
    decode    : out std_logic ;
    execute   : out std_logic ;
    increment : out std_logic
    );
  end component;
  component instruction_decoder is
    generic( N_BITS : integer := 8 );
    port (
    a_in     : in  std_logic_vector(N_BITS-1 downto 0);
    add      : out std_logic ;
    load     : out std_logic ;
    out_put  : out std_logic ;
    in_put   : out std_logic ;
    jumpz    : out std_logic ;
    jump     : out std_logic ;
    jumpnz   : out std_logic ;
    jumpc    : out std_logic ;
    jumpnc   : out std_logic ;
    sub      : out std_logic ;
    bitand   : out std_logic
    );
  end component;

  signal jump_condition_s : std_logic ;
  signal jump_not_taken : std_logic ;
  signal d_in_s   :  std_logic ;
  component ffd is
    port (
          clock  : in  std_logic ;
          d_in   : in  std_logic ;
          reset  : in  std_logic ;
          ce     : in  std_logic ;
          q_out  : out std_logic
    );
  end component;
begin
  ir_s    <= ir    ;
  clock_s <= clock ;
  carry_s <= carry ;
  zero_s  <= zero  ;
  en_st_s <= add_s or sub_s or bitand_s ;
  clr_s   <= clr   ;
  ce_s    <= ce    ;
  registro_2bit_0: register_2bit
                        port map    (clock  => clock_s    ,
                                     a_in   => carry_s    ,
                                     b_in   => zero_s     ,
                                     CE     => en_st_s    ,
                                     CLR    => clr_s      ,
                                     a_out  => carry_reg_s,
                                     b_out  => zero_reg_s);
  sequence_generator_0: sequence_generator
                        port map (clock     => clock_s   ,
                                  CE        => ce_s      ,
                                  clr       => clr_s     ,
                                  fetch     => fetch_s   ,
                                  decode    => decode_s  ,
                                  execute   => execute_s ,
                                  increment => increment_s);
  instruction_decoder_0: instruction_decoder
                        generic map (N_BITS => 8)
                        port    map (a_in   => ir_s     ,
                                     add    => add_s    ,
                                     load   => load_s   ,
                                     out_put=> output_s ,
                                     in_put => input_s  ,
                                     jumpz  => jumpz_s  ,
                                     jump   => jump_s   ,
                                     jumpc  => jumpc_s  ,
                                     jumpnz => jumpnz_s ,
                                     jumpnc => jumpnc_s ,
                                     sub    => sub_s    ,
                                     bitand => bitand_s);

  add_s_f    <= (decode_s or execute_s) and add_s    ;
  load_s_f   <= (decode_s or execute_s) and load_s   ;
  output_s_f <= (decode_s or execute_s) and output_s ;
  input_s_f  <= (decode_s or execute_s) and input_s  ;
  jumpz_s_f  <= (decode_s or execute_s) and jumpz_s  ;
  jump_s_f   <= (decode_s or execute_s) and jump_s   ;
  jumpc_s_f  <= (decode_s or execute_s) and jumpc_s  ;
  jumpnz_s_f <= (decode_s or execute_s) and jumpnz_s ;
  jumpnc_s_f <= (decode_s or execute_s) and jumpnc_s ;
  sub_s_f    <= (decode_s or execute_s) and sub_s    ;
  bitand_s_f <= (decode_s or execute_s) and bitand_s ;

 ffd_0 : ffd
          port map (clock => clock_s ,
                    d_in  => d_in_s  ,
                    reset => clr_s   ,
                    ce    => ce_s    ,
                    q_out => jump_not_taken);

 jump_condition_s   <= (jumpz_s_f  and zero_reg_s)        or
                       (jumpnz_s_f and (not zero_reg_s))  or
                       (jumpc_s_f  and carry_reg_s)       or
                       (jumpnc_s_f and (not carry_reg_s)) or jump_s_f ;


 d_in_s <= not jump_condition_s ;

 en_pc  <= (jump_condition_s and execute_s) or (increment_s and jump_not_taken);
 en_da  <= (load_s_f or add_s_f or sub_s_f or bitand_s_f or input_s_f) and execute_s ;
 en_in  <= fetch_s ;
 mux_c  <= input_s_f and output_s_f ;
 mux_b  <= load_s_f or add_s_f or bitand_s_f or sub_s_f ;
 mux_a  <= increment_s ;
 alu_s4 <= increment_s ;
 alu_s3 <= sub_s_f ;
 alu_s2 <= increment_s or sub_s_f ;
 alu_s1 <= (jump_s_f or jumpz_s_f or jumpnz_s_f or jumpc_s_f or jumpnc_s_f) or
            load_s_f or input_s_f or output_s_f ;
 alu_s0 <= (jump_s_f or jumpz_s_f or jumpnz_s_f or jumpc_s_f or jumpnc_s_f) or
            bitand_s_f or input_s_f or load_s_f ;
 ram    <= execute_s and output_s_f ;
end architecture;
