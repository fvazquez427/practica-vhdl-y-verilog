library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity decoder_tb is
end entity;

architecture decoder_tb_arch of decoder_tb is

  signal ir     :   std_logic_vector(7 downto 0);
  signal carry  :   std_logic;
  signal zero   :   std_logic;
  signal clock  :   std_logic;
  signal ce     :   std_logic;
  signal clr    :   std_logic;
  signal mux_a  :  std_logic;
  signal mux_b  :  std_logic;
  signal mux_c  :  std_logic;
  signal en_da  :  std_logic;
  signal en_pc  :  std_logic;
  signal en_in  :  std_logic;
  signal ram    :  std_logic;
  signal alu_s0 :  std_logic;
  signal alu_s1 :  std_logic;
  signal alu_s2 :  std_logic;
  signal alu_s3 :  std_logic;
  signal alu_s4 :  std_logic;

  component decoder is
    generic (N_BITS : integer := 8 );
    port (
    ir     : in  std_logic_vector(N_BITS-1 downto 0);
    carry  : in  std_logic;
    zero   : in  std_logic;
    clock  : in  std_logic;
    ce     : in  std_logic;
    clr    : in  std_logic;
    mux_a  : out std_logic;
    mux_b  : out std_logic;
    mux_c  : out std_logic;
    en_da  : out std_logic;
    en_pc  : out std_logic;
    en_in  : out std_logic;
    ram    : out std_logic;
    alu_s0 : out std_logic;
    alu_s1 : out std_logic;
    alu_s2 : out std_logic;
    alu_s3 : out std_logic;
    alu_s4 : out std_logic
    );
  end component;
  signal stop_simulation_s : BOOLEAN := FALSE ;
  constant PERIOD          : time  := 100 ns ;
begin

  decoder_0 : decoder port map (
                          ir     =>  ir    ,
                          carry  =>  carry ,
                          zero   =>  zero  ,
                          clock  =>  clock ,
                          ce     =>  ce    ,
                          clr    =>  clr   ,
                          mux_a  =>  mux_a ,
                          mux_b  =>  mux_b ,
                          mux_c  =>  mux_c ,
                          en_da  =>  en_da ,
                          en_pc  =>  en_pc ,
                          en_in  =>  en_in ,
                          ram    =>  ram   ,
                          alu_s0 =>  alu_s0,
                          alu_s1 =>  alu_s1,
                          alu_s2 =>  alu_s2,
                          alu_s3 =>  alu_s3,
                          alu_s4 =>  alu_s4
                          );
clockGeneration : process
  begin
    clock <= '1'      ;
    wait for PERIOD/2 ;
    clock <= '0'      ;
    wait for PERIOD/2 ;
    if stop_simulation_s then
      wait;
    end if;
  end process clockGeneration;

  test_process : process
  begin
    ce <= '1';
    carry <= '0';
    zero  <= '0';
    ir <= x"00" ;
    clr   <= '1';
    wait for PERIOD ;
    clr <= '0';
    wait for PERIOD * 4;
    ir <= x"a0";
    wait for PERIOD * 4;
    ir <= x"e0";
    wait for PERIOD * 4;
    ir <= x"00";
    wait for PERIOD * 4;
    ir <= x"e0";
    wait for PERIOD * 4;
    stop_simulation_s <= TRUE ;
    wait ;
  end process;


end architecture;
