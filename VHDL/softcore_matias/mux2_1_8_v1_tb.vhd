library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2_1_8_v1_tb is
end entity;

architecture mux2_1_8_v1_tbARCH of mux2_1_8_v1_tb is
  constant NUM_BITS : integer := 8 ;
  signal b_in :   std_logic_vector(NUM_BITS-1 downto 0);
  signal a_in :   std_logic_vector(NUM_BITS-1 downto 0);
  signal SEL  :   std_logic;
  signal z_out:   std_logic_vector(NUM_BITS-1 downto 0);

  component mux2_1_8 is
    generic(
      N_BITS : integer := 8
    );
     port (
     a_in : in  std_logic_vector(N_BITS-1 downto 0);
     b_in : in  std_logic_vector(N_BITS-1 downto 0);
     SEL  : in  std_logic;
     z_out: out std_logic_vector(N_BITS-1 downto 0)
    );
   end component mux2_1_8 ;
begin

  mux : mux2_1_8
                  generic map (N_BITS => NUM_BITS)

                  port map(a_in  => a_in,
                           b_in  => b_in,
                           SEl   => SEL ,
                           z_out => z_out);
  testProcess : process
  begin
    a_in <= "00001010";
    b_in <= "00001011";
    SEL <= '0';
    wait for 50 ns;
    SEL <= '1';
    wait for 50 ns;
    wait;
  end process;

end architecture;
