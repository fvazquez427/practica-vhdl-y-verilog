library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ffd_sin_reset is
  port (
  clock  : in  std_logic ;
  d_in   : in  std_logic ;
  q_out  : out std_logic
  );
end ffd_sin_reset;


architecture  ffd_sin_reset_arch of ffd_sin_reset  is
begin
  ffdProcess : process(clock)
  begin
      if (rising_edge (clock)) then
        if d_in = '1' then
          q_out <= '1';
        elsif d_in = '0' then
          q_out <= '0';
        end if;
      end if;
  end process;
end architecture;
