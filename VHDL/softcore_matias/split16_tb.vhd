library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity split16_tb is
end entity;

architecture split16_tb_arch of split16_tb is

  component split16 is
    generic ( N_BITS : integer := 16);
    port (
    a_in : in  std_logic_vector ( N_BITS-1 downto 0 );
    high : out std_logic_vector ( (N_BITS/2)-1 downto 0 );
    low  : out std_logic_vector ( (N_BITS/2)-1 downto 0 )
    );
  end component;
  constant N_BITS : integer := 16;
  signal  a_in :   std_logic_vector ( N_BITS-1 downto 0 );
  signal  high :   std_logic_vector ( (N_BITS/2)-1 downto 0 );
  signal  low  :   std_logic_vector ( (N_BITS/2)-1 downto 0 );
begin
  split : split16 port map (a_in => a_in ,
                            high => high ,
                            low  => low  );

test : process
begin
  a_in <= "11111111" & "00000000";
  wait for 50 ns;

  a_in <= "10100000" & "00001010";
  wait for 50 ns;

  a_in <= "01000000" & "00001010";
  wait for 50 ns;

  a_in <= "00000000" & "11111111";
  wait for 50 ns;

  a_in <= "11100000" & "00001010";
  wait for 50 ns;

  a_in <= "11111111" & "00000000";
  wait for 50 ns;
  wait ;
end process;

end architecture;
