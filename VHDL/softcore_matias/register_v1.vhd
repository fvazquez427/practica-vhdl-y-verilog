library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity register_v1 is
  generic(N_BITS : integer := 8);
  port (
  clock : in std_logic ;
  a_in  : in std_logic_vector(N_BITS-1 downto 0);
  CLR   : in std_logic ;
  CE    : in std_logic ;
  q_out : out std_logic_vector(N_BITS-1 downto 0)
  );
end register_v1;

architecture behavioral of register_v1 is
  signal q_out_s : std_logic_vector(N_BITS-1 downto 0);
begin
  register_process : process(clock, CE)
  begin
    if CLR = '1' then
      q_out_s <= (others => '0');
    elsif CE = '1' then
      if rising_edge(clock) then
        q_out_s <= a_in ;
      end if;
    end if;
  end process;

  q_out <= q_out_s ;

end architecture;
