library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
--contador en anillo
entity sequence_generator is
  port (
  clock     : in  std_logic ;
  CE        : in  std_logic ;
  clr       : in  std_logic ;
  fetch     : out std_logic ;
  decode    : out std_logic ;
  execute   : out std_logic ;
  increment : out std_logic
  );
end entity;

architecture behavioral of  sequence_generator is

  signal vector_estados_s : std_logic_vector(3 downto 0) := "1000";

begin
  sequence_process : process(clock, CE)
  begin
    if clr = '1' then
      vector_estados_s <= "1000" ;
    elsif CE = '1' then
      if rising_edge(clock) then
        vector_estados_s (3) <= vector_estados_s(0) ;
        vector_estados_s (2) <= vector_estados_s(3) ;
        vector_estados_s (1) <= vector_estados_s(2) ;
        vector_estados_s (0) <= vector_estados_s(1) ;
      end if;
    end if;
  end process;

  fetch     <= vector_estados_s (3);
  decode    <= vector_estados_s (2);
  execute   <= vector_estados_s (1);
  increment <= vector_estados_s (0);

end architecture;
