library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ripple_adder_v1_tb is
end entity;

architecture ripple_adder_v1_tbARHC of  ripple_adder_v1_tb is

  signal c_in :   std_logic;
  signal c_out:   std_logic;
  signal a_in :   std_logic_vector(7 downto 0);
  signal b_in :   std_logic_vector(7 downto 0);
  signal z_out:   std_logic_vector(7 downto 0);
  component ripple_adder_v1 is
    generic(
      N_BITS : integer := 8
    );
     port (
           c_in : in  std_logic;
           c_out: out std_logic;
           a_in : in  std_logic_vector(N_BITS-1 downto 0);
           b_in : in  std_logic_vector(N_BITS-1 downto 0);
           z_out: out std_logic_vector(N_BITS-1 downto 0)
           );
   end component ripple_adder_v1 ;
begin
  ripple_adder : ripple_adder_v1 port map (c_in  => c_in,
                                           c_out => c_out,
                                           a_in  => a_in,
                                           b_in  => b_in,
                                           z_out => z_out);
  testProcess : process
  begin
    a_in <= "00001000" ;
    b_in <= "00001100" ;
    c_in <= '0' ;
    wait for 50 ns;
    c_in <= '1';
    wait;
  end process;
end architecture;
